// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

// Looking for ways to speed up your queries, or scale easily with your serverless or edge functions?
// Try Prisma Accelerate: https://pris.ly/cli/accelerate-init

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider          = "postgresql"
  url               = env("DATABASE_URL")
  shadowDatabaseUrl = env("SHADOW_DATABASE_URL")
}

model Business {
  id        Int      @id @default(autoincrement())
  name      String   @unique
  address   String
  createdAt DateTime @default(now())
  isActive  Boolean  @default(true)
  users     User[]
  Task      Task[]
}

model User {
  id            Int           @id @default(autoincrement())
  email         String        @unique
  name          String
  password      String
  role          Role          @relation(fields: [roleId], references: [id], onDelete: SetDefault)
  roleId        Int           @default(2)
  business      Business      @relation(fields: [businessId], references: [id], onDelete: Cascade)
  businessId    Int
  tasks         Task[]
  createdTasks  TaskComment[] @relation("createdByUser")
  assignedTasks TaskComment[] @relation("assignedToUser")
  createdAt     DateTime      @default(now())
  isActive      Boolean       @default(true)
}

model Role {
  id          Int               @id @default(autoincrement())
  name        String
  users       User[]
  permissions Role_Permission[]
}

model Permission {
  id              Int               @id @default(autoincrement())
  name            String
  rolePermissions Role_Permission[]
}

model Role_Permission {
  id           Int        @id @default(autoincrement())
  role         Role       @relation(fields: [roleId], references: [id], onDelete: Cascade)
  roleId       Int
  permission   Permission @relation(fields: [permissionId], references: [id], onDelete: Cascade)
  permissionId Int
}

model Task {
  id           Int           @id @default(autoincrement())
  name         String        @db.VarChar(255)
  description  String
  priority     TaskPriority
  status       TaskStatus
  user         User          @relation(fields: [userId], references: [id], onDelete: Cascade)
  userId       Int
  business     Business      @relation(fields: [businessId], references: [id], onDelete: Cascade)
  businessId   Int
  dueDate      DateTime
  createdAt    DateTime      @default(now())
  taskComments TaskComment[]
}

enum TaskPriority {
  High   @map("High")
  Medium @map("Medium")
  Low    @map("Low")
}

enum TaskStatus {
  Pending    @map("Pending")
  InProgress @map("InProgress")
  Completed  @map("Completed")
  Cancelled  @map("Cancelled")
}

model TaskComment {
  id               Int      @id @default(autoincrement())
  comment          String
  task             Task     @relation(fields: [taskId], references: [id], onDelete: Cascade)
  taskId           Int
  createdByUserId  Int
  createdByUser    User     @relation("createdByUser", fields: [createdByUserId], references: [id])
  assignedToUserId Int
  assignedToUser   User     @relation("assignedToUser", fields: [assignedToUserId], references: [id])
  createdAt        DateTime @default(now())
}
