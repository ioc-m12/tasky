import prisma from "@/lib/db";
import { deleteBusinessSchema, registerBusinessSchema } from "@/types";
import { z } from "zod";

export async function createBusiness(
  data: z.infer<typeof registerBusinessSchema>,
) {
  return await prisma.business.create({
    data: {
      ...data,
    },
  });
}

export async function deleteBusiness(
  data: z.infer<typeof deleteBusinessSchema>,
) {
  return await prisma.business.delete({
    where: {
      ...data,
    },
  });
}
