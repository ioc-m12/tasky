import prisma from "@/lib/db";

export async function getRoleList() {
  return await prisma.role.findMany();
}

export async function getRoleById(roleId: number) {
  return await prisma.role.findFirstOrThrow({
    where: {
      id: roleId,
    },
  });
}
