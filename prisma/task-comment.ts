import { getSession } from "@/lib/auth";
import prisma from "@/lib/db";
import {
  createTaskCommentSchema,
  editTaskCommentSchema,
} from "@/types/TaskTypes";
import { z } from "zod";

export async function insertCommentToTask(
  data: z.infer<typeof createTaskCommentSchema>,
) {
  const userInfo = await getSession();
  if (!userInfo) throw new Error("No tenim informació de l'usuari");
  const userId = userInfo!.user.id;

  const userWantsAssignment = data.assignedToUserId !== undefined;
  const assignedToUserId = userWantsAssignment
    ? data.assignedToUserId!
    : userId;
  return await prisma.taskComment.create({
    data: {
      ...data,
      assignedToUserId: assignedToUserId,
      createdByUserId: userId,
    },
  });
}

export async function updateCommentInfo(
  data: z.infer<typeof editTaskCommentSchema>,
) {
  return await prisma.taskComment.update({
    where: {
      id: data.commentId,
    },
    data: {
      comment: data.comment,
      assignedToUser: {
        connect: { id: data.assignedToUserId },
      },
    },
  });
}
