import prisma from "@/lib/db";
import { registerUserSchema, updateUserSchema } from "@/types";
import { User } from "@prisma/client";
import bcrypt from "bcrypt";
import { z } from "zod";

export async function getUserById(userId: number) {
  return await prisma.user.findFirstOrThrow({
    where: {
      id: userId,
    },
  });
}

export async function getUserByEmail(email: string) {
  return await prisma.user.findUnique({
    where: {
      email: email,
    },
  });
}

export async function createUser(data: z.infer<typeof registerUserSchema>) {
  data.password = await bcrypt.hash(data.password, 10);
  return await prisma.user.create({
    data: {
      ...data,
    },
  });
}

export async function updateUserPassword(data: User) {
  return await prisma.user.update({
    where: {
      email: data.email,
    },
    data: {
      password: data.password,
    },
  });
}

export async function updateUser(data: z.infer<typeof updateUserSchema>) {
  return await prisma.user.update({
    where: {
      id: data.id,
    },
    data: data,
  });
}

export async function getUserList(businessId: number) {
  return await prisma.user.findMany({
    where: {
      businessId: businessId,
    },
  });
}
