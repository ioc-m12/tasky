/*
  Warnings:

  - Added the required column `assignedToUserId` to the `TaskComment` table without a default value. This is not possible if the table is not empty.
  - Added the required column `createdByUserId` to the `TaskComment` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "TaskComment" ADD COLUMN     "assignedToUserId" INTEGER NOT NULL,
ADD COLUMN     "createdByUserId" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "TaskComment" ADD CONSTRAINT "TaskComment_createdByUserId_fkey" FOREIGN KEY ("createdByUserId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TaskComment" ADD CONSTRAINT "TaskComment_assignedToUserId_fkey" FOREIGN KEY ("assignedToUserId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
