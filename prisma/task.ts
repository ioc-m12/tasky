import { getSession } from "@/lib/auth";
import prisma from "@/lib/db";
import { createTaskSchema, editTaskSchema } from "@/types/TaskTypes";
import { TaskStatus } from "@prisma/client";
import { z } from "zod";

export async function getTaskById(taskId: number) {
  return await prisma.task.findFirstOrThrow({
    where: {
      id: taskId,
    },
    include: {
      user: {
        select: {
          name: true,
        },
      },
      taskComments: {
        orderBy: {
          createdAt: "desc",
        },
        include: {
          createdByUser: {
            select: {
              name: true,
            },
          },
          assignedToUser: {
            select: {
              name: true,
            },
          },
        },
      },
    },
  });
}

export async function getTaskComments(taskId: number) {
  return await prisma.taskComment.findMany({
    where: {
      taskId: taskId,
    },
    orderBy: {
      createdAt: "desc",
    },
  });
}

export async function getTasks(businessId: number) {
  return await prisma.task.findMany({
    take: 20,
    where: {
      businessId: businessId,
      status: { not: "Completed" },
    },
    orderBy: {
      dueDate: "desc",
    },
  });
}

export async function searchTaskByTitle(businessId: number, taskTitle: string) {
  return await prisma.task.findMany({
    take: 20,
    where: {
      businessId: businessId,
      name: { contains: taskTitle, mode: "insensitive" },
    },
    orderBy: {
      dueDate: "desc",
    },
  });
}

export async function createTask(data: z.infer<typeof createTaskSchema>) {
  const userInfo = await getSession();
  if (!userInfo) throw new Error("No tenim informació de l'usuari");
  const userId = userInfo!.user.id;
  const createdTask = await prisma.task.create({
    data: {
      ...data,
      status: "Pending",
      businessId: userInfo!.user.businessId,
      userId: userId,
    },
  });

  return await prisma.taskComment.create({
    data: {
      comment: data.description,
      taskId: createdTask.id,
      createdByUserId: userId,
      assignedToUserId: userId,
    },
  });
}

export async function updateTask(data: z.infer<typeof editTaskSchema>) {
  const userInfo = await getSession();
  if (!userInfo) throw new Error("No tenim informació de l'usuari");
  const userBusinessId = userInfo!.user.businessId;
  const { taskId, ...updatedData } = data;

  return await prisma.task.update({
    where: {
      id: taskId,
      businessId: userBusinessId,
    },
    data: updatedData,
  });
}

export async function getAllTasksStatus() {
  return await prisma.task.groupBy({
    by: ["status"],
    _count: {
      status: true,
    },
  });
}

type TaskCount = {
  [_status in TaskStatus]?: number;
};

interface GroupedTasks {
  [date: string]: TaskCount;
}

export async function getTasksStatusGroupedByDate() {
  // It only returns the status of all the tasks grouped by date of the last week
  const oneWeekAgo = new Date();
  oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);

  const requestingUser = await getSession();
  const tasks = await prisma.task.findMany({
    where: {
      user: {
        businessId: requestingUser!.user.businessId,
      },
      taskComments: {
        some: {
          createdAt: {
            gte: oneWeekAgo,
          },
        },
      },
    },
    include: {
      taskComments: {
        orderBy: {
          createdAt: "desc",
        },
        take: 1,
      },
    },
  });

  const groupedPosts = tasks.reduce((acc: GroupedTasks, task) => {
    const date = task.taskComments[0].createdAt.toISOString().split("T")[0]; // Get the date part of the timestamp
    const status = task.status;

    // Initialize the counters if they don't exist
    if (!acc[date]) acc[date] = {};
    if (!acc[date][status]) acc[date][status] = 0;

    // Increment the counter
    acc[date][status]!++;

    return acc;
  }, {});

  return groupedPosts;
}
