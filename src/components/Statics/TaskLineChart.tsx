import { createLineChartTaskData } from "@/lib/taskStaticsHelpers";
import { IStatisticsData } from "@/types/TaskTypes";
import LineChart from "../ui/LineChart";

export default async function TaskLineChart({
  statisticsData,
}: {
  statisticsData: IStatisticsData;
}) {
  const lineChartData = await createLineChartTaskData(statisticsData);
  const lineChartOptions = {
    scales: {
      y: {
        suggestedMin: 0,
        suggestedMax: 12,
      },
    },
  };
  return (
    <LineChart
      lineChartData={lineChartData}
      lineChartOptions={lineChartOptions}
    />
  );
}
