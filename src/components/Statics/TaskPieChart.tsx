import { createPieChartTaskData } from "@/lib/taskStaticsHelpers";
import { IStatisticsData } from "@/types/TaskTypes";
import { ChartOptions } from "chart.js/auto";
import PieChart from "../ui/PieChart";

export default async function TaskPieChart({
  statisticsData,
}: {
  statisticsData: IStatisticsData;
}) {
  const pieChartData = await createPieChartTaskData(statisticsData);
  const pieChartOptions: ChartOptions<"pie"> = {
    plugins: {
      legend: {
        position: "left",
        labels: {
          padding: 5,
          font: {
            size: 17,
          },
        },
      },
    },
    maintainAspectRatio: false,
    aspectRatio: 1,
  };
  return (
    <PieChart pieChartData={pieChartData} pieChartOptions={pieChartOptions} />
  );
}
