import TaskStaticsSeleton from "@/components/Skeleton/TaskStaticsSeleton";
import { fetchStatistics } from "@/lib/taskStaticsHelpers";
import { IStatisticsData } from "@/types/TaskTypes";
import { Suspense } from "react";
import TaskLineChart from "./TaskLineChart";
import TaskPieChart from "./TaskPieChart";

export default async function TaskStatics() {
  const statisticsData: IStatisticsData = await fetchStatistics();

  return (
    <>
      <div className="flex justify-between gap-16 w-[90%]">
        <div
          className="p-6 bg-white shadow-lg w-1/2"
          style={{ borderRadius: "4rem" }}
        >
          <h2 className="text-2xl font-semibold m-5 pb-10 text-center">
            Visió general de les tasques
          </h2>
          <Suspense fallback={<TaskStaticsSeleton />}>
            <TaskPieChart statisticsData={statisticsData} />
          </Suspense>
        </div>
        <div
          className="p-6 bg-white shadow-lg rounded-3xl w-1/2"
          style={{ borderRadius: "4rem" }}
        >
          <h2 className="text-2xl font-semibold m-5 text-center">
            Activitat de tasques dels últims 7 dies
          </h2>
          <Suspense fallback={<TaskStaticsSeleton />}>
            <TaskLineChart statisticsData={statisticsData} />
          </Suspense>
        </div>
      </div>
    </>
  );
}
