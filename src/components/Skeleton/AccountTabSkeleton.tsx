import {
  DropdownMenu,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { BsThreeDotsVertical } from "react-icons/bs";

export default function AccountTabSkeleton() {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger className="flex animate-pulse">
        <span className="bg-primary rounded-md flex items-center p-3 bg-slate-200 w-14"></span>
        <div className="flex flex-col py-2 px-4 bg-secondary text-sm">
          <p className="bg-slate-200 w-32 h-5"></p>
          <p className="text-gray-600 bg-slate-200 w-32 h-5"></p>
        </div>
        <span className="bg-accent flex items-center">
          <BsThreeDotsVertical className="text-white text-xl" />
        </span>
      </DropdownMenuTrigger>
    </DropdownMenu>
  );
}
