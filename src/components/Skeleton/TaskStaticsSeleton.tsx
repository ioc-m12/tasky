export default function TaskStaticsSeleton() {
  return (
    <div
      role="status"
      className="w-full p-4 border border-gray-200 rounded shadow animate-pulse md:p-6 dark:border-gray-700"
    >
      <div className="flex items-baseline mt-4">
        <div className="w-full bg-gray-200 rounded-t-lg h-32 dark:bg-gray-700"></div>
        <div className="w-full h-36 ms-6 bg-gray-200 rounded-t-lg dark:bg-gray-700"></div>
        <div className="w-full bg-gray-200 rounded-t-lg h-52 ms-6 dark:bg-gray-700"></div>
        <div className="w-full h-44 ms-6 bg-gray-200 rounded-t-lg dark:bg-gray-700"></div>
        <div className="w-full bg-gray-200 rounded-t-lg h-40 ms-6 dark:bg-gray-700"></div>
        <div className="w-full bg-gray-200 rounded-t-lg h-52 ms-6 dark:bg-gray-700"></div>
        <div className="w-full bg-gray-200 rounded-t-lg h-60 ms-6 dark:bg-gray-700"></div>
      </div>
      <span className="sr-only">Carregant...</span>
    </div>
  );
}
