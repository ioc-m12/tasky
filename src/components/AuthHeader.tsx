import Logo from "@/components/Logo";

export default function AuthHeader({
  welcomeMessage,
  firstTime = false,
}: {
  welcomeMessage: string;
  firstTime?: boolean;
}) {
  return (
    <header>
      <Logo />
      <p className="text-sm font-semibold">
        {firstTime ? "Benvingut de nou" : "Benvinguts"}
      </p>
      <div className="flex items-center justify-center ml-auto mr-auto gap-4 mt-2 mb-8">
        <hr className="border-0 rounded bg-black w-5 h-1" />
        <h1 className="font-semibold text-3xl">{welcomeMessage}</h1>
        <hr className="border-0 rounded bg-black w-5 h-1 " />
      </div>
    </header>
  );
}
