"use client";

import { Button } from "@/components/ui/button";
import { useRouter } from "next/navigation";

export default function GoToHomeOrSignup() {
  const router = useRouter();

  return (
    <div className="flex-1 flex flex-col items-center justify-start gap-5">
      <Button
        className="w-full"
        onClick={() => router.push("/iniciar-sessio")}
        size="lg"
      >
        {"Inicia sessió"}
      </Button>
      <Button
        className="w-full"
        onClick={() => router.push("/registrar-empresa")}
        size="lg"
      >
        {"Registra't"}
      </Button>
    </div>
  );
}
