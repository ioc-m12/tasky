import Image from "next/image";
import { AspectRatio } from "@/components/ui/aspect-ratio";

export default function Logo() {
  return (
    <div className="w-60 ml-auto mr-auto">
      <AspectRatio ratio={21 / 9}>
        <Image
          src="/logo-tasky-complet.svg"
          alt="Image"
          width={300}
          height={300}
          className="rounded-md object-cover"
        />
      </AspectRatio>
    </div>
  );
}
