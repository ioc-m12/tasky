"use client";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import { Button } from "@/components/ui/button";
import { Calendar } from "@/components/ui/calendar";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Textarea } from "@/components/ui/textarea";
import { cn } from "@/lib/utils";
import { createTaskSchema } from "@/types/TaskTypes";
import { zodResolver } from "@hookform/resolvers/zod";
import { TaskComment } from "@prisma/client";
import { format } from "date-fns";
import { CalendarIcon } from "lucide-react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { CiSaveDown2 } from "react-icons/ci";
import { z } from "zod";

export default function TaskAddForm() {
  const router = useRouter();
  const form = useForm<z.infer<typeof createTaskSchema>>({
    resolver: zodResolver(createTaskSchema),
  });
  const [showCreatedTaskAlert, setCreatedTaskAlert] = useState(false);

  async function onSubmit(values: z.infer<typeof createTaskSchema>) {
    const response = await fetch("/api/protected/tasks", {
      method: "POST",
      body: JSON.stringify(values),
    });

    const data: TaskComment = await response.json();
    if (response.status !== 200) return;
    setCreatedTaskAlert(true);
    setTimeout(() => {
      setCreatedTaskAlert(false);
    }, 5000);

    router.push("/tauler/tasca-" + data.taskId);
  }

  return (
    <Form {...form}>
      {showCreatedTaskAlert && (
        <Alert variant={"success"}>
          <CiSaveDown2 className="h-4 w-4" />
          <AlertTitle>Tasca creada correctament!</AlertTitle>
          <AlertDescription>
            {
              "La tasca s'ha creat correctament, a continuació seràs redirigit per a veure el detall."
            }
          </AlertDescription>
        </Alert>
      )}
      <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Nom de la tasca</FormLabel>
              <FormControl>
                <Input
                  placeholder="Escriu el nom de la tasca"
                  autoFocus
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <div className="flex gap-2">
          <div className="w-1/2">
            <FormField
              control={form.control}
              name="dueDate"
              render={({ field }) => (
                <FormItem>
                  <FormLabel className="mt-2">Data Límit</FormLabel>
                  <Popover modal={true}>
                    <PopoverTrigger asChild>
                      <FormControl>
                        <Button
                          variant={"outline"}
                          className={cn(
                            "pl-3 text-left font-normal",
                            !field.value && "text-muted-foreground",
                          )}
                        >
                          {field.value ? (
                            format(field.value, "PPP")
                          ) : (
                            <span>Selecciona una data</span>
                          )}
                          <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                        </Button>
                      </FormControl>
                    </PopoverTrigger>
                    <PopoverContent className="w-auto p-0" align="start">
                      <Calendar
                        mode="single"
                        selected={field.value}
                        onSelect={field.onChange}
                        disabled={(date) => date < new Date()}
                        initialFocus
                      />
                    </PopoverContent>
                  </Popover>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>

          <FormField
            control={form.control}
            name="priority"
            render={({ field }) => (
              <FormItem className="w-1/2">
                <FormLabel>Prioritat</FormLabel>
                <Select
                  onValueChange={field.onChange}
                  defaultValue={field.value}
                >
                  <FormControl>
                    <SelectTrigger>
                      <SelectValue placeholder="Selecciona una prioritat" />
                    </SelectTrigger>
                  </FormControl>
                  <SelectContent>
                    <SelectItem value="High">Alta</SelectItem>
                    <SelectItem value="Medium">Mitja</SelectItem>
                    <SelectItem value="Low">Baixa</SelectItem>
                  </SelectContent>
                </Select>
                <FormMessage />
              </FormItem>
            )}
          />
        </div>
        <FormField
          control={form.control}
          name="description"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Descripció:</FormLabel>
              <FormControl>
                <Textarea
                  placeholder="Descriu la tasca aqui"
                  className="resize-none h-52"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button type="submit" className="flex w-full">
          Crear tasca
        </Button>
      </form>
    </Form>
  );
}
