import { Avatar, AvatarFallback } from "@/components/ui/avatar";
import {
  ITaskWithComments,
  editTaskCommentSchema,
  priorityNames,
  stateNames,
} from "@/types/TaskTypes";
import { ChangeEvent, useState } from "react";
import { FaEdit } from "react-icons/fa";
import { IoIosSave } from "react-icons/io";
import { MdCancel } from "react-icons/md";

export default function TaskDetails({ task }: { task: ITaskWithComments }) {
  const initialComment = task.taskComments[0];

  const [editMode, setEditMode] = useState(false);
  const [editedComment, setEditedComment] = useState(initialComment);
  const [displayedComment, setDisplayedComment] = useState(initialComment);

  const handleEdit = () => {
    setEditMode(true);
  };

  const handleInputChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setEditedComment((prevState) => ({
      ...prevState,
      comment: e.target.value,
    }));
  };

  const handleCancel = () => {
    setEditedComment(displayedComment);
    setEditMode(false);
  };

  const handleSave = async () => {
    const result = editTaskCommentSchema.safeParse({
      commentId: initialComment.id,
      assignedToUserId: initialComment.assignedToUserId,
      comment: editedComment.comment,
    });

    if (!result.success) {
      console.error("Error en actualitzar el comentari:", result.error);
      return;
    }

    const response = await fetch(`/api/protected/comments`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(result.data),
    });

    if (response.ok) {
      setEditMode(false);
      setDisplayedComment(editedComment);
    } else {
      console.error("Error en actualitzar el comentari:", response.status);
      console.error(await response.text());
    }
  };
  return (
    <div>
      <article className="space-y-10 max-w-7xl">
        <header className="rounded-lg shadow-lg p-8 bg-white">
          <h1 className="m-0 relative text-inherit tracking-[-0.04em] font-bold font-inherit">
            {task.name}
          </h1>

          <div className="mt-6 flex gap-16">
            <div className="flex flex-col">
              <h3 className="font-bold">Creat per: </h3>
              <p>{task.user.name}</p>
            </div>

            <div className="flex flex-col">
              <h3 className="font-bold">Prioritat:</h3>
              <p className="bg-hotpink-200 text-hotpink-100 rounded-full py-3 px-5">
                {
                  priorityNames.find(
                    (priority) => priority.type === task.priority,
                  )!.name
                }
              </p>
            </div>

            <div className="flex flex-col">
              <p className="font-bold">Data Assignat:</p>
              <p>
                {new Date(task.createdAt).toLocaleString("ca-ES", {
                  day: "2-digit",
                  month: "2-digit",
                  year: "numeric",
                  hour: "numeric",
                  minute: "numeric",
                })}
              </p>
            </div>

            <div className="flex flex-col">
              <p className="font-bold">Data Límit:</p>
              <p>
                {new Date(task.dueDate).toLocaleString("ca-ES", {
                  day: "2-digit",
                  month: "2-digit",
                  year: "numeric",
                  hour: "numeric",
                  minute: "numeric",
                })}
              </p>
            </div>
          </div>
          <p className="relative tracking-[-0.04em] font-semibold inline-block mt-5">
            Estat:
            <span className=" text-cornflowerblue-100 ml-5">
              {stateNames.find((state) => state.type === task.status)!.name}
            </span>
          </p>
        </header>

        <div className="flex gap-10">
          <div className="w-1/3">
            <h3 className="relative font-semibold inline-block text-gray-500 mb-3">
              Descripció:
            </h3>
            <p className="rounded-lg shadow-lg p-5 bg-white h-full">
              {task.description}
            </p>
          </div>

          <div className="w-2/3">
            <h3 className="relative font-semibold inline-block text-gray-500 mb-3">
              Comentaris:
            </h3>
            <ul className="bg-white shadow-lg rounded-lg p-5 h-full">
              <li className="flex justify-between gap-3">
                <Avatar>
                  <AvatarFallback>
                    {displayedComment.createdByUser.name
                      .split(" ")
                      .map((letter) => letter[0])
                      .join("")}
                  </AvatarFallback>
                </Avatar>
                <div className="grow w-full">
                  <h3 className="font-bold">
                    {displayedComment.createdByUser.name}{" "}
                    <span className="font-normal ml-1">
                      {timeAgo(new Date(displayedComment.createdAt))}
                    </span>
                  </h3>
                  {editMode ? (
                    <div className="flex">
                      <textarea
                        value={editedComment.comment}
                        onChange={handleInputChange}
                        className="flex-1 w-full"
                        style={{
                          height: "100px",
                          fontSize: "16px",
                          padding: "10px",
                          border: "1px solid #ccc",
                          borderRadius: "5px",
                          textAlign: "start",
                        }}
                      />
                    </div>
                  ) : (
                    <div className="self-start justify-end">
                      <p>{displayedComment.comment}</p>
                    </div>
                  )}
                </div>

                {editMode ? (
                  <div className="flex flex-col-reverse gap-3">
                    <button onClick={handleSave} className="text-2xl">
                      <IoIosSave />
                    </button>
                    <button
                      onClick={handleCancel}
                      className="text-2xl text-[#F44E8A]"
                    >
                      <MdCancel />
                    </button>
                  </div>
                ) : (
                  <button onClick={handleEdit} className="self-start">
                    <FaEdit />
                  </button>
                )}
              </li>

              {task.taskComments.slice(1).map((comment) => {
                return (
                  <li
                    key={comment.id}
                    className="flex justify-between gap-3 mt-7"
                  >
                    <Avatar>
                      <AvatarFallback>
                        {comment.createdByUser.name
                          .split(" ")
                          .map((letter) => letter[0])
                          .join("")}
                      </AvatarFallback>
                    </Avatar>
                    <div className="grow w-full">
                      <h3 className="font-bold">
                        {comment.createdByUser.name}{" "}
                        <span className="font-normal ml-1">
                          {timeAgo(new Date(comment.createdAt))}
                        </span>
                      </h3>
                      <p>{comment.comment}</p>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </article>
    </div>
  );
}

function timeAgo(date: Date): string {
  const now = new Date();
  const secondsPast = (now.getTime() - date.getTime()) / 1000;

  if (secondsPast < 60) {
    return "Just ara";
  }
  if (secondsPast < 3600) {
    return parseInt((secondsPast / 60).toString()) + " minuts enrere";
  }
  if (secondsPast <= 86400) {
    return parseInt((secondsPast / 3600).toString()) + " hores enrere";
  }

  let normalDate = date.toLocaleDateString("ca-ES", {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
  });

  return normalDate;
}
