"use client";

import { ITaskWithComments } from "@/types/TaskTypes";
import { notFound } from "next/navigation";
import { useEffect, useState } from "react";
import TaskDetails from "./TaskDetails";
import TaskDetailsSkeleton from "./TaskDetailsSkeleton";

interface ITaskDetails {
  taskId: number;
}

export default function TaskIDHandler({ taskId }: ITaskDetails) {
  const [task, setTask] = useState<ITaskWithComments | undefined>(undefined);
  const [loadingResponse, setLoadingResponse] = useState(true);

  useEffect(() => {
    const getTaskInfo = async () => {
      const response = await fetch(`/api/protected/tasks?task_id=${taskId}"`);
      const data: ITaskWithComments | undefined = await response.json();
      if (response.ok) setTask(data);
      setLoadingResponse(false);
    };

    getTaskInfo();
  }, [taskId]);

  if (!task && !loadingResponse) return notFound();
  else if (loadingResponse) return <TaskDetailsSkeleton />;
  else return <TaskDetails task={task!} />;
}
