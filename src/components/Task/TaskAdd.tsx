"use client";
import TaskAddForm from "@/components/Task/TaskAddForm";
import { Button } from "@/components/ui/button";
import {
  Sheet,
  SheetContent,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import { MdAssignmentAdd } from "react-icons/md";

export default function TaskAdd({ showButtonWithText = true }) {
  return (
    <Sheet>
      <SheetTrigger asChild>
        {showButtonWithText ? (
          <Button>
            <MdAssignmentAdd className="mr-3 text-xl" />
            Crear tasca
          </Button>
        ) : (
          <span>
            <MdAssignmentAdd className="text-3xl cursor-pointer" />
          </span>
        )}
      </SheetTrigger>
      <SheetContent>
        <SheetHeader>
          <SheetTitle className="text-3xl">Crear tasca</SheetTitle>
          <TaskAddForm />
        </SheetHeader>
      </SheetContent>
    </Sheet>
  );
}
