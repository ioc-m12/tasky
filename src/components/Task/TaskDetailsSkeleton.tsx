import { Avatar, AvatarFallback } from "@/components/ui/avatar";

export default function TaskDetailsSkeleton() {
  return (
    <div>
      <article className="space-y-10 max-w-7xl animate-pulse">
        <header className="rounded-lg shadow-lg p-8 bg-white">
          <h1 className="m-0 relative text-inherit tracking-[-0.04em] font-bold font-inherit bg-slate-200 w-96 h-12"></h1>

          <div className="mt-6 flex gap-16">
            <div className="flex flex-col">
              <h3 className="font-bold">Creat per: </h3>
              <p className="bg-slate-200 w-60 h-12"></p>
            </div>

            <div className="flex flex-col">
              <h3 className="font-bold">Prioritat:</h3>
              <p className="bg-slate-200 w-40 h-12 text-hotpink-100 rounded-full py-3 px-5"></p>
            </div>

            <div className="flex flex-col">
              <p className="font-bold">Data Assignat:</p>
              <p className="bg-slate-200 w-60 h-12"></p>
            </div>

            <div className="flex flex-col">
              <p className="font-bold">Data Límit:</p>
              <p className="bg-slate-200 w-60 h-12"></p>
            </div>
          </div>
          <div className="font-bold">
            Estat:
            <div className="bg-slate-200 w-40 h-12"></div>
          </div>
        </header>

        <div className="flex gap-10">
          <div className="w-1/3">
            <h3 className="relative font-semibold inline-block text-gray-500 mb-3">
              Descripció:
            </h3>
            <p className="rounded-lg shadow-lg p-5  h-full bg-slate-200 w-96 "></p>
          </div>

          <div className="w-2/3">
            <h3 className="relative font-semibold inline-block text-gray-500 mb-3">
              Comentaris:
            </h3>
            <ul className="bg-white shadow-lg rounded-lg p-5 h-full flex flex-col gap-5">
              {[...Array(3)].map((_, colIndex) => (
                <li className="flex justify-between gap-3" key={colIndex}>
                  <Avatar>
                    <AvatarFallback className="bg-slate-200 w-96 h-12"></AvatarFallback>
                  </Avatar>
                  <div className="grow w-full">
                    <h3 className="font-bold bg-slate-200 w-72 h-12"></h3>

                    <div className="self-start justify-end">
                      <p className="bg-slate-200 w-96 h-14"></p>
                    </div>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </article>
    </div>
  );
}
