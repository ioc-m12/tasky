"use client";
import TableSkeleton from "@/components/Skeleton/TableSkeleton";
import "@/components/Tables/custom-theme-table.css";
import { AgGridReact } from "ag-grid-react";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { CiSaveDown2 } from "react-icons/ci";
import { Alert, AlertDescription, AlertTitle } from "../ui/alert";

type Role = {
  id: number;
  name: string;
};

export default function UserList() {
  const router = useRouter();
  const [userList, setUserList] = useState<undefined>();
  const [roles, setRoles] = useState<Role[]>([{ id: 1, name: "" }]);
  const [showAlertUserModified, setAlertUserModified] = useState(false);
  const [isLoading, setLoadingState] = useState(true);

  const defaultColDef = {
    editable: true,
  };

  const columnDefs = [
    { field: "id", editable: false },
    {
      field: "email",
    },
    { field: "name", headerName: "Nom complet" },
    {
      field: "roleId",
      headerName: "Rol",
      valueFormatter: (params: any) => {
        if (params.value === undefined) "";
        return roles!.find((role) => params.value === role.id)!.name;
      },
      width: 150,
      cellEditor: "agSelectCellEditor",
      cellEditorParams: {
        values: roles.map((role) => role.id),
        valueListMaxHeight: 220,
      },
    },
    { field: "isActive", headerName: "Estat" },
  ];

  useEffect(() => {
    const getUsersData = async () => {
      const userListResponse = await fetch("/api/protected/users");
      const isUserAdmin = userListResponse.status !== 401;
      if (!isUserAdmin) router.push("/tauler");

      const userListData = await userListResponse.json();
      setUserList(userListData);

      const rolesResponse = await fetch("/api/protected/roles");
      const rolesData = await rolesResponse.json();
      setRoles(rolesData);
      setLoadingState(false);
    };

    getUsersData();
  }, []);

  function updateUserCell(param: any) {
    fetch("/api/protected/users", {
      method: "PUT",
      body: JSON.stringify(param.data),
    }).then((res) => res.json());

    setAlertUserModified(true);
    setTimeout(() => {
      setAlertUserModified(false);
    }, 5000);
  }

  if (isLoading)
    return (
      <div className="ag-theme-quartz w-full h-[500px] border-2 rounded-md">
        <TableSkeleton />
      </div>
    );

  return (
    <>
      {showAlertUserModified && (
        <Alert variant={"success"}>
          <CiSaveDown2 className="h-4 w-4" />
          <AlertTitle>Usuari actualizat!</AlertTitle>
          <AlertDescription>
            {"S'ha modificat la informació de l'usuari correctament."}
          </AlertDescription>
        </Alert>
      )}
      <div className="ag-theme-quartz w-full h-[500px] border-2 rounded-md">
        {!isLoading && (
          <AgGridReact
            rowData={userList}
            columnDefs={columnDefs}
            defaultColDef={defaultColDef}
            overlayLoadingTemplate="Carregant usuaris..."
            onCellValueChanged={updateUserCell}
            autoSizeStrategy={{ type: "fitCellContents" }}
          />
        )}
      </div>
    </>
  );
}
