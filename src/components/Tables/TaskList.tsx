"use client";
import TableSkeleton from "@/components/Skeleton/TableSkeleton";
import "@/components/Tables/custom-theme-table.css";
import { priorityNames, stateNames } from "@/types/TaskTypes";
import { AgGridReact } from "ag-grid-react";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";

export default function TaskList() {
  const [taskList, setTaskList] = useState<undefined>();
  const [isLoading, setLoadingState] = useState(true);
  const router = useRouter();

  const defaultColDef = {
    editable: false,
  };

  const columnDefs = [
    { field: "id" },
    {
      field: "name",
      headerName: "Titol",
    },
    { field: "description", headerName: "Descripció", flex: 1 },
    {
      field: "priority",
      headerName: "Prioritat",
      valueFormatter: (params: any) => {
        if (params.value === undefined) "";
        return priorityNames.find((priority) => priority.type == params.value)!
          .name;
      },
    },
    {
      field: "status",
      headerName: "Estat",
      valueFormatter: (params: any) => {
        if (params.value === undefined) "";
        return stateNames.find((state) => state.type == params.value)!.name;
      },
    },
    {
      field: "dueDate",
      headerName: "Data limit",
      valueFormatter: (params: any) => {
        if (params.value === undefined) "";
        return new Date(params.value).toLocaleDateString();
      },
    },
    {
      field: "createdAt",
      headerName: "Data creació",
      valueFormatter: (params: any) => {
        if (params.value === undefined) "";
        const date = new Date(params.value);
        return (
          date.toLocaleDateString() + " a les " + date.toLocaleTimeString()
        );
      },
    },
  ];

  useEffect(() => {
    fetch("/api/protected/tasks/list")
      .then((response) => response.json())
      .then((data) => {
        setTaskList(data), setLoadingState(false);
      });
  }, []);

  if (isLoading)
    return (
      <div>
        <TableSkeleton />
      </div>
    );
  return (
    <>
      <div className="ag-theme-quartz w-full h-[500px] border-2 rounded-md">
        <AgGridReact
          rowData={taskList}
          columnDefs={columnDefs}
          defaultColDef={defaultColDef}
          overlayLoadingTemplate="Carregant tasques..."
          overlayNoRowsTemplate="No hi han tasques assignades a l'empresa"
          autoSizeStrategy={{ type: "fitCellContents" }}
          onRowClicked={(event) =>
            router.push("/tauler/tasca-" + event.data.id)
          }
        />
      </div>
    </>
  );
}
