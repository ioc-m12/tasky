"use client";
import "chart.js/auto";
import { ChartOptions } from "chart.js/auto";
import { Line } from "react-chartjs-2";

export default function LineChart({
  lineChartData,
  lineChartOptions,
}: {
  lineChartData: any;
  lineChartOptions: ChartOptions<"line">;
}) {
  return (
    <div className="pt-6 w-full max-w-2xl mx-auto">
      <Line data={lineChartData} options={lineChartOptions} />
    </div>
  );
}
