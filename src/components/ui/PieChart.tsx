"use client";
import "chart.js/auto";
import { ChartOptions } from "chart.js/auto";
import { Pie } from "react-chartjs-2";

export default function PieChart({
  pieChartData,
  pieChartOptions,
}: {
  pieChartData: any;
  pieChartOptions: ChartOptions<"pie">;
}) {
  return (
    <div className="w-full max-w-xl mx-auto" style={{ height: "250px" }}>
      <Pie data={pieChartData} options={pieChartOptions} />
    </div>
  );
}
