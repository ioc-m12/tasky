import { cn } from "@/lib/utils";
import { HTMLInputTypeAttribute } from "react";
import { FieldError, useFormContext } from "react-hook-form";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import { IconType } from "react-icons/lib";
import { Button } from "./button";
import { FormControl, FormField, FormItem } from "./form";
import { Input } from "./input";

interface IInputWithIcon extends React.InputHTMLAttributes<HTMLInputElement> {
  name: string;
  Icon: IconType;
  errorMsg: FieldError | undefined;
  passwordState?: [boolean, React.Dispatch<React.SetStateAction<boolean>>];
  type: HTMLInputTypeAttribute | undefined;
}

export default function InputWithIcon({
  name,
  Icon,
  errorMsg,
  passwordState,
  type,
  ...props
}: IInputWithIcon) {
  const { control } = useFormContext(); // Use useFormContext to access the form control

  return (
    <FormField
      control={control}
      name={name}
      render={({ field }) => (
        <FormItem>
          <FormControl className="bg-gray-300">
            <div className="relative rounded-md">
              <Icon className="absolute left-4 top-1/2 h-4 w-4 -translate-y-1/2 transform text-gray-400" />
              <Input
                {...field}
                className={cn(
                  "pl-10 py-6 bg-gray-300",
                  errorMsg &&
                    "border-red-600 focus-visible:ring-red-600 focus-visible:border-input ",
                )}
                type={type == "password" && passwordState![0] ? "text" : type}
                {...props}
              />

              {passwordState && (
                <Button
                  type="button"
                  variant="ghost"
                  size="sm"
                  className="absolute right-0 top-0 h-full px-3 py-2 hover:bg-transparent"
                  onClick={() => passwordState[1]((prev) => !prev)}
                >
                  {passwordState[0] ? (
                    <FaEyeSlash
                      className="h-4 w-4 text-gray-400"
                      aria-hidden="true"
                    />
                  ) : (
                    <FaEye
                      className="h-4 w-4 text-gray-500"
                      aria-hidden="true"
                    />
                  )}
                  <span className="sr-only">
                    {passwordState[0]
                      ? "Amaga la contrasenya"
                      : "Mostra la contrasenya"}
                  </span>
                </Button>
              )}
            </div>
          </FormControl>
          {errorMsg && (
            <p className="text-red-600 text-left">{errorMsg.message}</p>
          )}
        </FormItem>
      )}
    />
  );
}
