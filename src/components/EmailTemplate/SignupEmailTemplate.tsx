interface EmailTemplateProps {
  token: string;
  baseURL: string;
}

export const SignupEmailTemplate = ({ token, baseURL }: EmailTemplateProps) => `
  <div>
    <h1>Crea el teu compte com a treballador aqui:</h1>
    <a href="${baseURL}/registrar-treballador?token=${token}">
      ${baseURL}/registrar-treballador?token=${token}
    </a>
  </div>
`;
