interface EmailTemplateProps {
  token: string;
  baseURL: string;
}

export const ResetEmailTemplate = ({ token, baseURL }: EmailTemplateProps) =>
  `
    <div>
      <h1>Reseteja la contrasenya aqui:</h1>
      <a href="${baseURL}/resetejar-contrasenya?token=${token}">
        ${baseURL}/resetejar-contrasenya?token=${token}
      </a>
    </div>
`;
