import { createContext } from "react";

export interface IGlobalContext {
  user: {
    username: string;
    role: string;
  };
}

const GlobalContext = createContext<IGlobalContext | null>(null);
export default GlobalContext;
