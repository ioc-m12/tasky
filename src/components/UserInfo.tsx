"use client";

import GlobalContext, { IGlobalContext } from "@/components/GlobalContext";
import { useEffect, useState } from "react";

export default function UserInfo({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const [userInfo, setUserInfo] = useState<IGlobalContext | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("/api/users");
      const data = await response.json();
      const userInfo = {
        username: data.username,
        role: data.role,
      };
      setUserInfo({ user: userInfo });
    };

    fetchData();
  }, []);

  return (
    <GlobalContext.Provider value={userInfo}>{children}</GlobalContext.Provider>
  );
}
