"use client";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { IoPersonAdd } from "react-icons/io5";
import { z } from "zod";
import { Button } from "./ui/button";

import { Form } from "@/components/ui/form";
import { inviteUserSchema } from "@/types";
import { useState } from "react";
import { CiSaveDown2 } from "react-icons/ci";
import { FaEnvelope } from "react-icons/fa";
import InputWithIcon from "./ui/InputWithIcon";
import { Alert, AlertDescription, AlertTitle } from "./ui/alert";

export default function InviteWorker() {
  const [showAlertInvitedUser, setInvitedUser] = useState(false);
  const [showModal, setModalState] = useState(false);

  const form = useForm<z.infer<typeof inviteUserSchema>>({
    resolver: zodResolver(inviteUserSchema),
    defaultValues: {
      email: "",
    },
  });

  async function onSubmit(data: z.infer<typeof inviteUserSchema>) {
    await fetch("/api/create-user", {
      method: "POST",
      body: JSON.stringify(data),
    });

    setInvitedUser(true);
    setTimeout(() => {
      setInvitedUser(false);
      setModalState(false);
    }, 5000);
  }

  return (
    <Dialog open={showModal} onOpenChange={setModalState}>
      <DialogTrigger asChild>
        <Button>
          <IoPersonAdd className="mr-3" />
          Afegir treballador
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Afegir un treballador</DialogTitle>
          <DialogDescription>
            <p>
              Introdueix l&apos;email del treballador que vols invitar i se li
              enviarà un correu amb un enllaç per a registrar-se.
            </p>
            {showAlertInvitedUser && (
              <Alert variant={"success"}>
                <CiSaveDown2 className="h-4 w-4" />
                <AlertTitle>Usuari afegit!</AlertTitle>
                <AlertDescription>
                  {"S'ha afegit a l'usuari correctament."}
                </AlertDescription>
              </Alert>
            )}
            <Form {...form}>
              <form onSubmit={form.handleSubmit(onSubmit)} className="mt-3">
                <InputWithIcon
                  name="email"
                  placeholder="Correu electrònic"
                  type="email"
                  Icon={FaEnvelope}
                  errorMsg={form.formState.errors.email}
                />
                <Button type="submit" className="mt-3">
                  Afegit treballador
                </Button>
              </form>
            </Form>
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
}
