import { Suspense } from "react";
import AccountTab from "./AccountTab";
import PanelTaskSearcher from "./PanelTaskSearcher";

export default function ControlPanelHeader() {
  return (
    <header className="py-6 pl-10 pr-20 flex justify-between">
      <div>
        <PanelTaskSearcher />
      </div>
      <div>
        <Suspense fallback={<p>Que tal broo</p>}>
          <AccountTab />
        </Suspense>
      </div>
    </header>
  );
}
