import dynamic from "next/dynamic";
import Image from "next/image";
import Link from "next/link";

const PanelNavigationItems = dynamic(() => import("./PanelNavigationItems"), {
  loading: () => <p>Carregant...</p>,
});

export default function PanelSidebar() {
  return (
    <aside className="h-svh pt-8 px-3 bg-[#FAFFFD]">
      <Link href={"/"}>
        <Image
          src="/logo-tasky.svg"
          alt="Image"
          width={70}
          height={70}
          className="rounded-md object-cover"
        />
      </Link>

      <PanelNavigationItems />
    </aside>
  );
}
