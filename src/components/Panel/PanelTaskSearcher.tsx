"use client";
import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from "@/components/ui/command";
import { cn } from "@/lib/utils";
import { useRouter } from "next/navigation";
import { useState } from "react";

interface Task {
  id: number;
  name: string;
  description: string;
}

export default function PanelTaskSearcher() {
  const [open, setOpen] = useState(false);
  const [search, setSearchText] = useState("");
  const [taskList, setTaskList] = useState<Task[]>([]);
  const router = useRouter();

  async function searchTasks(taskTitle: string) {
    const response = await fetch(
      "/api/protected/tasks/search?task_title=" + taskTitle,
    );
    setTaskList(await response.json());
    setOpen(true);
  }

  return (
    <Command className={cn("bg-secondary", open ? "flex-col" : "flex-row")}>
      <CommandInput
        placeholder="Buscador de tasques..."
        className="border-none w-72"
        value={search}
        onValueChange={(taskTitle) => {
          const isUserSearchingTask = taskTitle && taskTitle.length > 2;
          isUserSearchingTask ? searchTasks(taskTitle) : setOpen(false);
          setSearchText(taskTitle);
        }}
      />
      <CommandList>
        {open && (
          <>
            <CommandEmpty>No s&apos;ha trobat cap tasca.</CommandEmpty>
            <CommandGroup heading="Suggerències">
              {taskList.map((task) => (
                <CommandItem
                  key={task.id}
                  onSelect={() => {
                    setOpen(false);
                    setSearchText("");
                    router.push("/tauler/tasca-" + task.id);
                  }}
                >
                  {task.name}
                </CommandItem>
              ))}
            </CommandGroup>
          </>
        )}
      </CommandList>
    </Command>
  );
}
