"use client";
import GlobalContext from "@/components/GlobalContext";
import AccountTabSkeleton from "@/components/Skeleton/AccountTabSkeleton";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import Link from "next/link";
import { useContext } from "react";
import { BsThreeDotsVertical } from "react-icons/bs";
import { CiLogout } from "react-icons/ci";

export default function AccountTab() {
  const globalData = useContext(GlobalContext);
  const fullName = globalData?.user.username;
  const role = globalData?.user.role;
  let firstLettersOfName = "";
  if (fullName)
    firstLettersOfName =
      fullName!.split(" ")[0][0] + fullName!.split(" ")[1][0];

  if (!globalData) return <AccountTabSkeleton />;

  return (
    <DropdownMenu>
      <DropdownMenuTrigger className="flex">
        <span className="bg-primary rounded-md flex items-center p-3">
          {firstLettersOfName}
        </span>
        <div className="flex flex-col py-2 px-4 bg-secondary text-sm">
          <p>{fullName}</p>
          <p className="text-gray-600">{role}</p>
        </div>
        <span className="bg-accent flex items-center">
          <BsThreeDotsVertical className="text-white text-xl" />
        </span>
      </DropdownMenuTrigger>
      <DropdownMenuContent>
        {role === "administrador" && (
          <DropdownMenuItem className="focus:bg-transparent focus:text-black">
            <Link
              href={"/tauler/gestionar-treballadors"}
              className="no-underline"
            >
              <p>Gestionar treballadors</p>
            </Link>
          </DropdownMenuItem>
        )}

        <DropdownMenuSeparator />
        <DropdownMenuItem className="focus:bg-transparent focus:text-black">
          <Link
            prefetch={false}
            href={"/api/auth/logout"}
            className="flex items-center no-underline gap-2"
          >
            <CiLogout />
            <p>Tancar sessió</p>
          </Link>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}
