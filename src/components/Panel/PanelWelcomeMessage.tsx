"use client";
import GlobalContext from "@/components/GlobalContext";
import { useContext } from "react";

export default function PanelWelcomeMessage() {
  const globalData = useContext(GlobalContext);
  const username = globalData?.user.username.split(" ")[0];

  return (
    <h1>
      Benvingut <b>{username}!</b>
    </h1>
  );
}
