"use client";
import TaskAdd from "@/components/Task/TaskAdd";
import { cn } from "@/lib/utils";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useState } from "react";
import { IoBarChartSharp } from "react-icons/io5";
import { MdDashboard, MdManageAccounts } from "react-icons/md";

export default function PanelNavigationItems() {
  const pathname = usePathname();
  const [activeButton, setActiveButton] = useState("/tauler");

  const items = [
    {
      Icon: MdDashboard,
      url: "/tauler",
    },
    {
      Icon: IoBarChartSharp,
      url: "/tauler/estadistiques",
    },
    {
      Icon: TaskAdd,
      url: "",
    },
    {
      Icon: MdManageAccounts,
      url: "/tauler/gestionar-treballadors",
    },
  ];

  return (
    <ul className="w-full text-center mt-10 flex flex-col gap-7">
      {items.map((item, index) => {
        if (index === 2) {
          return (
            <li
              key={index}
              className="p-3 pb-2 rounded-lg w-fit mx-auto bg-secondary"
            >
              <TaskAdd showButtonWithText={false} />
            </li>
          );
        }
        const isCurrentPage = pathname === item.url;
        return (
          <li key={item.url} className="p-3">
            <Link
              prefetch={false}
              href={item.url}
              aria-disabled={!isCurrentPage}
              tabIndex={isCurrentPage ? 0 : -1}
              onClick={() => setActiveButton(item.url)}
              className={cn(
                isCurrentPage && "pointer-events-none",
                activeButton === item.url && "bg-primary",
              )}
            >
              <div
                className={cn(
                  "p-3 rounded-lg w-fit mx-auto",
                  activeButton === item.url ? "bg-primary" : "bg-secondary",
                )}
              >
                {<item.Icon className="text-3xl" />}
              </div>
            </Link>
          </li>
        );
      })}
    </ul>
  );
}
