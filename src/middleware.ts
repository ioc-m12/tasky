import { getSession, updateSession } from "@/lib/auth";
import { NextRequest, NextResponse } from "next/server";

export async function middleware(request: NextRequest) {
  const isRouteProtected =
    request.nextUrl.pathname.startsWith("/api/protected") ||
    request.nextUrl.pathname.startsWith("/tauler");

  const isAuthenticated = await getSession();
  const loginPage = "/iniciar-sessio";
  if (isRouteProtected && !isAuthenticated) {
    return NextResponse.redirect(new URL(loginPage, request.url));
  }

  const isLoginPage = request.nextUrl.pathname.startsWith(loginPage);
  if (isLoginPage && isAuthenticated) {
    return NextResponse.redirect(new URL("/tauler", request.url));
  }

  return await updateSession(request);
}
