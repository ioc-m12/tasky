export default function AuthLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="bg-[url('/fons-auth.jpg')] bg-center bg-no-repeat bg-cover h-svh flex items-center">
      <section className="p-12 bg-white w-full md:w-1/4 rounded-md ml-auto mr-auto drop-shadow-lg text-center">
        {children}
      </section>
    </div>
  );
}
