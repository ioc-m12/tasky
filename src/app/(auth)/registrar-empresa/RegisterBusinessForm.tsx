"use client";
import InputWithIcon from "@/components/ui/InputWithIcon";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import { Button } from "@/components/ui/button";
import { Form } from "@/components/ui/form";
import {
  CategoriseResponseType,
  deleteBusinessSchema,
  registerCombinedSchema,
} from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { FaLocationArrow } from "react-icons/fa";
import { FaBriefcase, FaBuilding, FaEnvelope, FaLock } from "react-icons/fa6";
import { MdOutlineErrorOutline } from "react-icons/md";
import { z } from "zod";

export default function RegisterBusinessForm() {
  const [showPassword, setShowPassword] = useState(false);
  const [isFetchError, setFetchError] = useState<string | undefined>(undefined);
  let router = useRouter();

  const form = useForm<z.infer<typeof registerCombinedSchema>>({
    resolver: zodResolver(registerCombinedSchema),
    defaultValues: {
      user: {
        name: "",
        email: "",
        password: "",
        roleId: 1,
        businessId: 1,
      },
      business: {
        name: "",
        address: "",
      },
    },
  });

  async function onSubmit(values: z.infer<typeof registerCombinedSchema>) {
    const response = await fetch("/api/businesses", {
      method: "POST",
      body: JSON.stringify(values.business),
    });

    const data = await response.json();
    let categorizedResponse = CategoriseResponseType(data);
    if (categorizedResponse.type == "error") {
      setFetchError(categorizedResponse.error);
      return;
    }
    const createdBusiness = deleteBusinessSchema.safeParse(data);
    if (!createdBusiness.success) {
      setFetchError("Error desconegut a l'hora de llegir l'empresa nova.");
      return;
    }

    values.user.businessId = createdBusiness.data.id;
    const responseUser = await fetch("/api/users", {
      method: "POST",
      body: JSON.stringify(values.user),
    });
    categorizedResponse = CategoriseResponseType(responseUser);
    if (categorizedResponse.type == "error") {
      setFetchError(categorizedResponse.error);
      // If there are errors when creating a user, delete the created business
      await fetch("/api/businesses", {
        method: "DELETE",
        body: JSON.stringify(values.user.businessId),
      });
      return;
    }
    router.push("/iniciar-sessio");
  }

  return (
    <Form {...form}>
      {isFetchError && (
        <Alert className="mb-5" variant={"destructive"}>
          <MdOutlineErrorOutline className="h-4 w-4" />
          <AlertTitle>S&apos;ha trobat un error al registrar-se.</AlertTitle>
          <AlertDescription>{isFetchError}</AlertDescription>
        </Alert>
      )}

      <div className="max-w-md mx-auto">
        <form
          className="flex flex-col gap-5"
          onSubmit={form.handleSubmit(onSubmit)}
        >
          <InputWithIcon
            name="business.name"
            placeholder="Nom de l'empresa"
            type="text"
            Icon={FaBuilding}
            autoFocus
            errorMsg={form.formState.errors.business?.name}
            autoComplete="organization"
          />

          <InputWithIcon
            name="business.address"
            placeholder="Direcció física de l'empresa"
            type="text"
            Icon={FaLocationArrow}
            errorMsg={form.formState.errors.business?.address}
            autoComplete="address-line1"
          />

          <InputWithIcon
            name="user.name"
            placeholder="Nom i cognoms del cap de l'empresa"
            type="text"
            Icon={FaBriefcase}
            errorMsg={form.formState.errors.user?.name}
            autoComplete="name"
          />

          <InputWithIcon
            name="user.email"
            placeholder="Correu electrònic"
            type="email"
            Icon={FaEnvelope}
            errorMsg={form.formState.errors.user?.email}
            autoComplete="email"
          />

          <InputWithIcon
            name="user.password"
            placeholder="Contrasenya"
            type={showPassword ? "text" : "password"}
            Icon={FaLock}
            errorMsg={form.formState.errors.user?.password}
            autoComplete="new-password"
            passwordState={[showPassword, setShowPassword]}
          />

          <Button type="submit" size="lg">
            Registra&apos;t
          </Button>
        </form>
      </div>
    </Form>
  );
}
