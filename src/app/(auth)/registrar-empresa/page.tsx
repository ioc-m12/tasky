import AuthHeader from "@/components/AuthHeader";
import Link from "next/link";
import RegisterBusinessForm from "./RegisterBusinessForm";

export default function RegistrarEmpresa() {
  return (
    <>
      <div className="justify-center items-center max-w-md mx-auto">
        <AuthHeader welcomeMessage="Registra't" />

        <RegisterBusinessForm />

        <p className="my-3">o</p>
        <Link href={"/iniciar-sessio"} className="no-underline">
          {"Tens compte? Inicia sessió aquí"}
        </Link>
      </div>
    </>
  );
}
