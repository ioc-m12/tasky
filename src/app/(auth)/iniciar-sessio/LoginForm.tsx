"use client";
import InputWithIcon from "@/components/ui/InputWithIcon";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import { Button } from "@/components/ui/button";
import { Form } from "@/components/ui/form";
import { CategoriseResponseType, loginSchema } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { FaBriefcase, FaLock } from "react-icons/fa";
import { MdOutlineErrorOutline } from "react-icons/md";
import { z } from "zod";

export default function LoginForm() {
  const [showPassword, setShowPassword] = useState(false);
  const [isFetchError, setFetchError] = useState<string | undefined>(undefined);
  let router = useRouter();

  const form = useForm<z.infer<typeof loginSchema>>({
    resolver: zodResolver(loginSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  });

  async function onSubmit(values: z.infer<typeof loginSchema>) {
    const response = await fetch("/api/auth/login", {
      method: "POST",
      body: JSON.stringify(values),
    });
    const data = await response.json();
    const typedResponse = CategoriseResponseType(data);
    let isResponseError =
      typedResponse.type == "error" || typedResponse.type == "unknown";
    if (isResponseError) {
      setFetchError(
        typedResponse.type == "error"
          ? typedResponse.error
          : "Error desconegut.",
      );
      return;
    }
    router.push("/tauler");
  }

  return (
    <Form {...form}>
      {isFetchError && (
        <Alert className="mb-5" variant={"destructive"}>
          <MdOutlineErrorOutline className="h-4 w-4" />
          <AlertTitle>
            S&apos;ha trobat un error a l&apos;iniciar sessió
          </AlertTitle>
          <AlertDescription>{isFetchError}</AlertDescription>
        </Alert>
      )}

      <form
        className="flex flex-col gap-8"
        onSubmit={form.handleSubmit(onSubmit)}
      >
        <InputWithIcon
          name="email"
          placeholder="Correu electrònic"
          type="email"
          Icon={FaBriefcase}
          autoFocus
          errorMsg={form.formState.errors.email}
          autoComplete="email"
        />

        <InputWithIcon
          name="password"
          placeholder="Contrasenya"
          type="password"
          Icon={FaLock}
          errorMsg={form.formState.errors.password}
          autoComplete="password"
          passwordState={[showPassword, setShowPassword]}
        />

        <Link
          href={"/resetejar-contrasenya"}
          className="-mt-8 text-gray-600 self-end no-underline"
        >
          Has oblidat la contrasenya?
        </Link>
        <Button type="submit" size={"lg"}>
          Inicia sessió
        </Button>
      </form>
    </Form>
  );
}
