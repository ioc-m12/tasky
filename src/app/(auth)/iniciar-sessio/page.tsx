import AuthHeader from "@/components/AuthHeader";
import { Metadata } from "next";
import Link from "next/link";
import LoginForm from "./LoginForm";

export const metadata: Metadata = {
  title: "Inicia sessió | Tasky",
  description: "Inicia sessió per accedir al software Tasky.",
};

export default function Page() {
  return (
    <>
      <AuthHeader welcomeMessage="Inicia sessió" firstTime />

      <LoginForm />
      <p className="my-3">o</p>
      <Link href={"/registrar-empresa"} className="no-underline">
        Registra&apos;t com a empresa
      </Link>
    </>
  );
}
