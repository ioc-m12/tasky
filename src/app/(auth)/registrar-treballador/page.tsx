import AuthHeader from "@/components/AuthHeader";
import { Metadata } from "next";
import Link from "next/link";
import { Suspense } from "react";
import HandleSignupLogic from "./HandleSignupLogic";
export const metadata: Metadata = {
  title: "Registrar Treballador | Tasky",
  description: "Registra un Treballador per assignarli tasques.",
};

export default function page() {
  return (
    <>
      <AuthHeader welcomeMessage="Registra´t" />
      <Suspense fallback={<div>Carregant...</div>}>
        <HandleSignupLogic />
      </Suspense>
      <p className="my-3">o</p>
      <Link href={"/iniciar-sessio"} className="no-underline">
        {"Tens compte? Inicia sessió aquí"}
      </Link>
    </>
  );
}
