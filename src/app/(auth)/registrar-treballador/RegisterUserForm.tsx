"use client";
import InputWithIcon from "@/components/ui/InputWithIcon";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import { Button } from "@/components/ui/button";
import { Form } from "@/components/ui/form";
import { CategoriseResponseType, registerUserSchema } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { FaBriefcase, FaEnvelope, FaLock } from "react-icons/fa";
import { MdOutlineErrorOutline } from "react-icons/md";
import { z } from "zod";

export interface IRegisterUserForm {
  userData: IUserData;
}
export interface IUserData {
  email: string;
  businessId: number;
  roleId: number;
}

export default function RegisterUserForm({ userData }: IRegisterUserForm) {
  const [showPassword, setShowPassword] = useState(false);
  const [isFetchError, setFetchError] = useState<string | undefined>(undefined);
  let router = useRouter();

  const form = useForm<z.infer<typeof registerUserSchema>>({
    resolver: zodResolver(registerUserSchema),
    defaultValues: {
      name: "",
      businessId: userData.businessId,
      roleId: userData.roleId,
      email: userData.email,
      password: "",
    },
  });

  async function onSubmit(values: z.infer<typeof registerUserSchema>) {
    const response = await fetch("/api/users", {
      method: "POST",
      body: JSON.stringify(values),
    });
    const data = await response.json();
    const typedResponse = CategoriseResponseType(data);
    if (typedResponse.type == "error") {
      setFetchError(typedResponse.error);
      return;
    }
    router.push("/iniciar-sessio");
  }

  return (
    <Form {...form}>
      {isFetchError && (
        <Alert className="mb-5" variant={"destructive"}>
          <MdOutlineErrorOutline className="h-4 w-4" />
          <AlertTitle>
            S&apos;ha trobat un error a l&apos;iniciar sessió
          </AlertTitle>
          <AlertDescription>{isFetchError}</AlertDescription>
        </Alert>
      )}

      <form
        className="flex flex-col gap-8"
        onSubmit={form.handleSubmit(onSubmit)}
      >
        <InputWithIcon
          name="name"
          placeholder="Nom i cognoms"
          type="text"
          Icon={FaBriefcase}
          errorMsg={form.formState.errors.name}
          autoComplete="name"
        />

        <InputWithIcon
          name="email"
          placeholder="Correu electrònic"
          type="email"
          Icon={FaEnvelope}
          errorMsg={form.formState.errors.email}
          value={userData.email}
          disabled={true}
        />

        <InputWithIcon
          name="password"
          placeholder="Contrasenya"
          type="password"
          Icon={FaLock}
          errorMsg={form.formState.errors.password}
          autoComplete="new-password"
          passwordState={[showPassword, setShowPassword]}
        />

        <Button type="submit" size={"lg"}>
          Registra&apos;t
        </Button>
      </form>
    </Form>
  );
}
