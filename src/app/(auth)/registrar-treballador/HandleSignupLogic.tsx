"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import RegisterUserForm, { IUserData } from "./RegisterUserForm";

export default function HandleSignupLogic() {
  const searchParams = useSearchParams();
  const router = useRouter();
  const token = searchParams.get("token");

  const [isValidToken, setIsValidToken] = useState(false);
  const [userData, setUserData] = useState<IUserData>();

  async function checkToken() {
    const response = await fetch(`/api/create-user?token=${token}`);
    const data = await response.json();

    setIsValidToken(data.success);
    if (!data.success) router.push("/iniciar-sessio");
    setUserData({
      email: data.email,
      businessId: data.businessId,
      roleId: data.roleId,
    });
  }

  useEffect(() => {
    if (token) {
      checkToken();
    } else {
      router.push("/iniciar-sessio");
    }
  }, [token]);

  return isValidToken && <RegisterUserForm userData={userData!} />;
}
