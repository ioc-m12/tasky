import InputWithIcon from "@/components/ui/InputWithIcon";
import { Button } from "@/components/ui/button";
import { Form } from "@/components/ui/form";
import { sendRecoverPasswordSchema } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import Link from "next/link";
import { useForm } from "react-hook-form";
import { FaEnvelope } from "react-icons/fa6";
import { z } from "zod";

export default function ForgotPassword() {
  const form = useForm<z.infer<typeof sendRecoverPasswordSchema>>({
    resolver: zodResolver(sendRecoverPasswordSchema),
    defaultValues: {
      email: "",
    },
  });

  async function onSubmit(values: z.infer<typeof sendRecoverPasswordSchema>) {
    const response = await fetch("/api/auth/reset-password", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    });

    const data = await response.json();
    if (response.ok) {
      alert(data.message);
    } else {
      alert(data.error);
    }
  }

  return (
    <>
      <div className="max-w-md mx-auto rounded-md">
        <p className="text-sm font-semibold">Has oblidat la contrasenya?</p>
        <div className="flex items-center justify-center ml-auto mr-auto gap-4 mt-2 mb-8">
          <hr className="border-0 rounded bg-black w-5 h-1 " />
          <h2 className="font-semibold text-2xl">Restableix la contrasenya</h2>
          <hr className="border-0 rounded bg-black w-5 h-1 " />
        </div>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-5">
            <InputWithIcon
              name="email"
              Icon={FaEnvelope}
              errorMsg={form.formState.errors.email}
              type="email"
              autoComplete="email"
              placeholder="Correu electrònic"
            />
            <Button type="submit" size={"lg"} className="mb-3">
              Restableix contrasenya
            </Button>
          </form>
        </Form>
        <Link href={"/iniciar-sessio"} className="no-underline">
          {"Torna a iniciar sessió"}
        </Link>
      </div>
    </>
  );
}
