"use client";
import InputWithIcon from "@/components/ui/InputWithIcon";
import { Button } from "@/components/ui/button";
import { Form } from "@/components/ui/form";
import { changePasswordSchema } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { useRouter, useSearchParams } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { FaLock } from "react-icons/fa6";
import { z } from "zod";

export default function ResetPassword() {
  const router = useRouter();
  const searchParams = useSearchParams();
  const token = searchParams.get("token");
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showRepeatedPassword, setShowRepeatedPassword] = useState(false);
  const form = useForm<z.infer<typeof changePasswordSchema>>({
    resolver: zodResolver(changePasswordSchema),
    defaultValues: {
      newPassword: "",
      repeatedPassword: "",
      token: token!,
    },
  });

  async function onSubmit(values: z.infer<typeof changePasswordSchema>) {
    const response = await fetch("/api/auth/reset-password", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    });

    if (response.ok) {
      router.push("/iniciar-sessio");
    } else {
      alert("Hi ha hagut un error a l'intentar canviar la contrasenya");
    }
  }
  return (
    <div className="max-w-md mx-auto rounded-md">
      <div className="flex items-center justify-center ml-auto mr-auto gap-4 mt-2 mb-8">
        <hr className="border-0 rounded bg-black w-5 h-1 " />
        <h2 className="font-semibold text-2xl">Restablir la contrasenya</h2>
        <hr className="border-0 rounded bg-black w-5 h-1 " />
      </div>

      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-5">
          <InputWithIcon
            name="newPassword"
            Icon={FaLock}
            errorMsg={form.formState.errors.newPassword}
            type="password"
            autoComplete="new-password"
            placeholder="Nova contrasenya"
            passwordState={[showNewPassword, setShowNewPassword]}
            autoFocus
          />
          <InputWithIcon
            name="repeatedPassword"
            Icon={FaLock}
            errorMsg={form.formState.errors.repeatedPassword}
            type="password"
            placeholder="Repeteix la contrasenya"
            passwordState={[showRepeatedPassword, setShowRepeatedPassword]}
          />

          <Button type="submit" size={"lg"}>
            Canvia contrasenya
          </Button>
        </form>
      </Form>
    </div>
  );
}
