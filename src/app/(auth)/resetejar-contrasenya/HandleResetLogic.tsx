"use client";
import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import ForgotPassword from "./ForgotPassword";
import ResetPassword from "./ResetPassword";

export default function HandleResetLogic() {
  const searchParams = useSearchParams();
  const token = searchParams.get("token");

  const [isValidToken, setIsValidToken] = useState(false);

  async function checkToken() {
    const response = await fetch(`/api/auth/reset-password?token=${token}`);
    const data = await response.json();

    setIsValidToken(data.success);
  }

  useEffect(() => {
    if (token) {
      checkToken();
    }
  }, [token]);

  if (token && isValidToken) {
    return <ResetPassword />;
  }

  return <ForgotPassword />;
}
