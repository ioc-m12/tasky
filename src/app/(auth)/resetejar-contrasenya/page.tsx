import Logo from "@/components/Logo";

import { Suspense } from "react";
import HandleResetLogic from "./HandleResetLogic";

export default function Page() {
  return (
    <>
      <div className="text-center">
        <Logo />
        <Suspense fallback={<div>Carregant...</div>}>
          <HandleResetLogic />
        </Suspense>
      </div>
    </>
  );
}
