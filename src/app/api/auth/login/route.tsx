import { getUserByEmail } from "@/db/user";
import { createExpirationDate, encrypt } from "@/lib/auth";
import {
  getSafeRequestData,
  raiseError,
  raiseSuccess,
} from "@/lib/requestHelpers";
import { loginSchema } from "@/types";
import bcrypt from "bcrypt";
import { cookies } from "next/headers";

export async function POST(request: Request) {
  let authData;
  try {
    authData = await getSafeRequestData(request, loginSchema);
  } catch (error) {
    if (error instanceof Error) return raiseError(error.message, 400);
    return raiseError("Unknown error", 500);
  }

  const user = await getUserByEmail(authData.email);

  // Validations to check if the user is correct and can be authenticated
  if (!user)
    return raiseError("Correu electrònic o contrasenya incorrectes", 404);
  const passwordMatches = await bcrypt.compare(
    authData.password,
    user.password,
  );
  if (!passwordMatches)
    return raiseError("Correu electrònic o contrasenya incorrectes", 404);
  if (!user.isActive) return raiseError("L'usuari no està actiu", 403);

  // Create the session
  const expires = createExpirationDate();
  const session = await encrypt({ user, expires });

  // Save the session (visibly only to servers)
  cookies().set("session", session, { expires, httpOnly: true });
  return raiseSuccess("Authentication successful");
}
