import { logout } from "@/lib/auth";
import { NextResponse } from "next/server";
export async function GET(request: Request) {
  logout();
  return NextResponse.redirect(new URL("/iniciar-sessio", request.url));
}
