import { ResetEmailTemplate } from "@/components/EmailTemplate/ResetEmailTemplate";
import { getUserByEmail, updateUserPassword } from "@/db/user";
import { decrypt, encrypt } from "@/lib/auth";
import { sendEmail } from "@/lib/email";
import { raiseError, raiseSuccess } from "@/lib/requestHelpers";
import { RequestRecoverPasswordSchema } from "@/types";
import { User } from "@prisma/client";
import bcrypt from "bcrypt";

export async function GET(req: Request) {
  const url = new URL(req.url);
  const searchParams = new URLSearchParams(url.searchParams);
  const token = searchParams.get("token");
  if (!token) return raiseError("No s'ha proporcionat cap token.", 400);

  // Check if the token recived is valid
  let data: { email: string } | Object;
  try {
    data = await decrypt(token);
  } catch (error) {
    return raiseError("Aquest token no és vàlid.", 400);
  }
  const haveDataId = "userInfo" in data;
  if (!haveDataId)
    return raiseError(
      "Aquest token és correcte però no conte cap camp identificatiu.",
      400,
    );

  // Token is correct
  return Response.json({ success: true });
}

export async function POST(req: Request) {
  const body = await req.json();
  const reqData = RequestRecoverPasswordSchema.safeParse(body);
  if (!reqData.success) return raiseError("Format incorrecte.", 400);
  const data = reqData.data;
  // If user want to reset his password
  if ("token" in data) return resetPassword(data.token, data.newPassword);

  const user = await getUserByEmail(data.email);
  if (!user)
    return raiseError("Cap usuari existeix amb l'email especificat", 404);

  // If user requests to recive a email to reset his password
  const baseURL = new URL(req.url).origin;
  return sendResetEmail(user, data.email, baseURL);
}

async function sendResetEmail(user: User, to: string, baseURL: string) {
  // Expiration date of 1 hr
  const expirationDate = new Date(Date.now() + 3600 * 1000 * 1);
  const userInfo = { email: user.email };
  const token = await encrypt({ userInfo, expirationDate });

  try {
    await sendEmail(
      to,
      "Reseteja la teva contrasenya | Tasky",
      ResetEmailTemplate({ token: token, baseURL: baseURL }),
    );
    return raiseSuccess("S'ha enviat el correu correctament.");
  } catch (error) {
    return raiseError("Ha hagut un error al intentar enviar el correu", 500);
  }
}

async function resetPassword(token: string, newPassword: string) {
  const { userInfo } = await decrypt(token);
  if (!userInfo.email) return raiseError("Token incorrecte.", 400);
  const user = await getUserByEmail(userInfo.email);
  if (!user)
    return raiseError("Cap usuari existeix amb l'email especificat", 404);

  const newPasswordEncrypted = await bcrypt.hash(newPassword, 10);
  user.password = newPasswordEncrypted;

  const result = await updateUserPassword(user);
  return Response.json(result);
}
