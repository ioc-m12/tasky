import { createBusiness, deleteBusiness } from "@/db/business";
import { makeDBOperation } from "@/lib/requestHelpers";
import { deleteBusinessSchema, registerBusinessSchema } from "@/types";

export async function POST(request: Request) {
  return makeDBOperation(
    request,
    registerBusinessSchema,
    createBusiness,
    "Error desconegut a l'hora de crear l'empresa",
  );
}

export async function DELETE(request: Request) {
  return makeDBOperation(
    request,
    deleteBusinessSchema,
    deleteBusiness,
    "Error desconegut a l'hora d'eliminar l'empresa",
  );
}
