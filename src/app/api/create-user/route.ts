import { IUserData } from "@/app/(auth)/registrar-treballador/RegisterUserForm";
import { SignupEmailTemplate } from "@/components/EmailTemplate/SignupEmailTemplate";
import { decrypt, encrypt, getSession } from "@/lib/auth";
import { sendEmail } from "@/lib/email";
import { raiseError, raiseSuccess } from "@/lib/requestHelpers";
import { inviteUserSchema } from "@/types";
/* 
This file contains all the logic needed to follow the flow to create a user,
but in any case it returns the info of a user or create one (this is handle by /api/users).

The GET is needed to allow a user / worker to create an account (filling a form)
by checking that it provides a valid token.

The POST is needed to send the invitation / email to a worker to access to the
signup page.
*/

export async function GET(req: Request) {
  const url = new URL(req.url);
  const searchParams = new URLSearchParams(url.searchParams);
  const token = searchParams.get("token");
  if (!token) return raiseError("No s'ha proporcionat cap token.", 400);

  // Check if the token recived is valid
  let data: { userInfo: IUserData };
  try {
    data = await decrypt(token);
  } catch (error) {
    return raiseError("Aquest token no és vàlid.", 400);
  }
  const haveDataId = "userInfo" in data;
  if (!haveDataId)
    return raiseError(
      "Aquest token és correcte però no conte cap camp identificatiu.",
      400,
    );

  // Token is correct
  return Response.json({
    success: true,
    email: data.userInfo.email,
    businessId: data.userInfo.businessId,
    roleId: data.userInfo.roleId,
  });
}

export async function POST(req: Request) {
  const body = await req.json();
  const reqData = inviteUserSchema.safeParse(body);
  if (!reqData.success) return raiseError("Format incorrecte.", 400);
  const data = reqData.data;

  // token is needed to know what businessId is
  const session = await getSession();
  if (!session || session.user.roleId != 1)
    return raiseError(
      "Has d'iniciar sessió i ser administrador per invitar a un membre a la teva empresa.",
      400,
    );

  const baseURL = new URL(req.url).origin;
  return sendInvitationEmail(session.user.businessId, data.email, baseURL);
}

async function sendInvitationEmail(
  businessId: number,
  email: string,
  baseURL: string,
) {
  // Expiration date of 8 hr
  const expirationDate = new Date(Date.now() + 3600 * 1000 * 1 * 8);
  const userInfo = { businessId: businessId, email: email, roleId: 2 };
  const token = await encrypt({ userInfo, expirationDate });

  try {
    await sendEmail(
      email,
      "Crea el teu compte com a treballador | Tasky",
      SignupEmailTemplate({ token: token, baseURL: baseURL }),
    );
    return raiseSuccess("S'ha enviat el correu correctament.");
  } catch (error) {
    return raiseError("Ha hagut un error al intentar enviar el correu", 500);
  }
}
