import { getRoleList } from "@/db/role";

export async function GET() {
  return Response.json(await getRoleList());
}
