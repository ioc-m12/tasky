import { getUserList, updateUser } from "@/db/user";
import { getSession } from "@/lib/auth";
import { makeDBOperation, raiseError } from "@/lib/requestHelpers";
import { updateUserSchema } from "@/types";

export async function GET() {
  const userInfo = await getSession();
  const isUserAdmin = userInfo!.user.roleId == 1;
  if (!isUserAdmin) return raiseError("Current user is not admin", 401);
  return Response.json(await getUserList(userInfo!.user.businessId));
}

export async function PUT(request: Request) {
  return makeDBOperation(
    request,
    updateUserSchema,
    updateUser,
    "Error desconegut a l'hora d'actualitzar l'usuari",
  );
}
