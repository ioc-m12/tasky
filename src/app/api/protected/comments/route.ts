import { getTaskById } from "@/db/task";
import { insertCommentToTask, updateCommentInfo } from "@/db/task-comment";
import { getUserById } from "@/db/user";
import { getSession } from "@/lib/auth";
import { sendEmail } from "@/lib/email";
import { makeDBOperation, raiseError } from "@/lib/requestHelpers";
import {
  createTaskCommentSchema,
  editTaskCommentSchema,
} from "@/types/TaskTypes";

export async function POST(request: Request) {
  const commentCreated = await makeDBOperation(
    request,
    createTaskCommentSchema,
    insertCommentToTask,
    "Error intern desconegut a l'hora de crear un comentari.",
  );

  const isCommentCreated = commentCreated.status === 200;
  if (!isCommentCreated) return commentCreated; // If not is created return the error

  const commentCreatedData = await commentCreated.json();
  const data = createTaskCommentSchema.parse(commentCreatedData);
  let taskInfo;
  try {
    taskInfo = await getTaskById(data.taskId);
  } catch (err) {
    return raiseError("Hi ha hagut un error en obtenir l'ultima tasca", 500);
  }

  /* If the user who creates the comment is different than the author of the task, send an
   email to the author to inform about the progress of the task
   */
  const commentCreatorInfo = await getSession();
  const isTheAuthorOfTask = taskInfo.userId === commentCreatorInfo!.user.id;
  if (isTheAuthorOfTask) return Response.json(commentCreatedData);

  const subject = `Comentari nou a la tasca ${data.taskId}`;
  const message = `Algú ha creat un comentari a la tasca ${data.taskId}`;
  const taskCreatorInfo = await getUserById(taskInfo.userId);
  try {
    await sendEmail(taskCreatorInfo.email, subject, message);
    return Response.json(commentCreatedData);
  } catch (error) {
    return raiseError(
      "Hi ha hagut un error a l'intentar enviar el correu al creador de la tasca, però s'ha creat el comentari",
      500,
    );
  }
}

export async function PUT(request: Request) {
  return await makeDBOperation(
    request,
    editTaskCommentSchema,
    updateCommentInfo,
    "Error intern desconegut a l'hora d'actualitzar un comentari",
  );
}
