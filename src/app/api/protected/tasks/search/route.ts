import { searchTaskByTitle } from "@/db/task";
import { getSession } from "@/lib/auth";
import { raiseError } from "@/lib/requestHelpers";

export async function GET(request: Request) {
  const userInfo = await getSession();
  const userBusinessId = userInfo!.user.businessId;

  const url = new URL(request.url);
  const searchParams = new URLSearchParams(url.searchParams);
  const taskTitle = searchParams.get("task_title");
  if (!taskTitle)
    return raiseError("No s'ha proporcionat cap nom de tasca.", 400);

  const tasks = await searchTaskByTitle(userBusinessId, taskTitle);
  return Response.json(tasks);
}
