import { createTask, getTaskById, updateTask } from "@/db/task";
import { getSession } from "@/lib/auth";
import { makeDBOperation, raiseError } from "@/lib/requestHelpers";
import { createTaskSchema, editTaskSchema } from "@/types/TaskTypes";
import { Task } from "@prisma/client";

export async function GET(request: Request) {
  const url = new URL(request.url);
  const searchParams = new URLSearchParams(url.searchParams);
  const taskIdString = searchParams.get("task_id");
  if (!taskIdString)
    return raiseError("No s'ha proporcionat cap id de tasca.", 400);

  const taskId = parseInt(taskIdString);

  let task: Task;
  try {
    task = await getTaskById(taskId);
  } catch (err) {
    return raiseError("La tasca especificada no existeix.", 404);
  }

  // Check if the user is trying to see a task that is not of his business
  const currentUserInfo = await getSession();
  const currentUserBusinessId = currentUserInfo!.user.businessId;
  if (task.businessId !== currentUserBusinessId)
    return raiseError(
      "Estàs intentant veure una tasca que no és de la teva empresa.",
      401,
    );

  return Response.json(task);
}

export async function POST(request: Request) {
  return makeDBOperation(
    request,
    createTaskSchema,
    createTask,
    "Error intern desconegut a l'hora de crear la tasca.",
  );
}

export async function PUT(request: Request) {
  return makeDBOperation(
    request,
    editTaskSchema,
    updateTask,
    "Error intern desconegut a l'hora de modificar l'informació de la tasca.",
  );
}
