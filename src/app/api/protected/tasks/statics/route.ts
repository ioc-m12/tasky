import { getAllTasksStatus, getTasksStatusGroupedByDate } from "@/db/task";

export async function GET() {
  const tasksByStatus = await getAllTasksStatus();
  const taskStatusOfLastWeek = await getTasksStatusGroupedByDate();
  return Response.json({
    success: true,
    tasksByStatus: tasksByStatus,
    tasksStatusGroupedByDateOfLastWeek: taskStatusOfLastWeek,
  });
}
