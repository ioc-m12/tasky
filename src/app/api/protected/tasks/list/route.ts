import { getTasks } from "@/db/task";
import { getSession } from "@/lib/auth";

export async function GET() {
  const userInfo = await getSession();
  const userBusinessId = userInfo!.user.businessId;
  return Response.json(await getTasks(userBusinessId));
}
