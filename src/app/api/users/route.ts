import { getRoleById } from "@/db/role";
import { createUser } from "@/db/user";
import { getSession } from "@/lib/auth";
import { makeDBOperation, raiseError } from "@/lib/requestHelpers";
import { registerUserSchema } from "@/types";
import { Role } from "@prisma/client";

export async function GET() {
  const userInfo = await getSession();
  let role: Role;
  try {
    role = await getRoleById(userInfo!.user.roleId);
  } catch (err) {
    return raiseError("No s'ha trobat el rol de l'usuari", 404);
  }
  const userData = { username: userInfo!.user.name, role: role.name };
  return Response.json(userData);
}

export async function POST(request: Request) {
  return makeDBOperation(
    request,
    registerUserSchema,
    createUser,
    "Error desconegut a l'hora de crear l'usuari",
  );
}
