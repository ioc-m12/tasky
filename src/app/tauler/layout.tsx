import UserInfo from "@/components/UserInfo";
import dynamic from "next/dynamic";

// Dynamic import
const PanelHeader = dynamic(() => import("@/components/Panel/PanelHeader"));
const PanelSidebar = dynamic(() => import("@/components/Panel/PanelSidebar"));

export default function Layout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <div className="flex">
      <UserInfo>
        <PanelSidebar />
        <div className="grow">
          <PanelHeader />
          <main className="pl-10 pr-20 mt-6">{children}</main>
        </div>
      </UserInfo>
    </div>
  );
}
