import PanelWelcomeMessage from "@/components/Panel/PanelWelcomeMessage";
import TaskList from "@/components/Tables/TaskList";
import TaskAdd from "@/components/Task/TaskAdd";

export default function page() {
  const currentDate = new Date().toLocaleDateString("ca-ES", {
    year: "numeric",
    month: "long",
    day: "numeric",
  });

  return (
    <div>
      <p className="text-[#666666]">{currentDate}</p>
      <PanelWelcomeMessage />
      <span className="flex flex-col items-end my-5">
        <TaskAdd />
      </span>
      <TaskList />
    </div>
  );
}
