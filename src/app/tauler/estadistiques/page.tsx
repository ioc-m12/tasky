import TaskStatics from "@/components/Statics/TaskStatics";

export default async function page() {
  return (
    <>
      <div>
        <h1 className="font-bold">Estadístiques</h1>
        <p className="text-l mt-3">
          Troba aquí una visió completa del teu progrés i de les tasques
          pendents.
        </p>
        <p className="pt-9 pb-10 text-xl font-semibold">
          Utilitza els gràfics següents per entendre millor la teva
          productivitat i identificar àrees <br />
          on pots millorar i gestionar les teves tasques de manera eficient.
        </p>
      </div>

      <TaskStatics />
    </>
  );
}
