import InviteWorker from "@/components/InviteWorker";
import UserList from "@/components/Tables/UserList";

export default function page() {
  return (
    <>
      <h1 className="font-semibold">Gestionar treballadors</h1>
      <p className="text-l mt-3">
        Gestiona aquí de forma fàcil els treballadors de la teva empresa.
      </p>
      <p className="pt-10 text-xl font-semibold">
        A la llista inferior trobaràs informació del teu equip, on pots
        modificar les dades fàcilment. <br />
        Per modificar qualsevol camp, només cal fer doble clic sobre el mateix.
        <span className="block text-gray-400 font-normal">
          (excepte pel camp &quot;Id&quot;)
        </span>
      </p>
      <div className="flex flex-col items-end gap-4">
        <InviteWorker />
        <UserList />
      </div>
    </>
  );
}
