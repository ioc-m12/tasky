import TaskIDHandler from "@/components/Task/TaskIDHandler";
import { Suspense } from "react";

export default function page({ params }: { params: { taskId: string } }) {
  const userTaskId = parseInt(params.taskId.split("tasca-")[1]);

  return (
    <Suspense>
      <TaskIDHandler taskId={userTaskId} />
    </Suspense>
  );
}
