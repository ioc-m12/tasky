import Logo from "@/components/Logo";
import Link from "next/link";
import Layout from "./(auth)/layout";

export default function notFound() {
  return (
    <Layout>
      <Logo />
      <h1 className="text-5xl">404</h1>
      <div className="flex items-center justify-center ml-auto mr-auto gap-4 mt-2 mb-2">
        <hr className="border-0 rounded bg-black w-5 h-1" />
        <h2 className="text-3xl">Pàgina no trobada</h2>
        <hr className="border-0 rounded bg-black w-5 h-1" />
      </div>

      <p className="mb-5">
        No hem trobat la pàgina específicada, <br />
        encara així sempre pots tornar amb nosaltres.
      </p>

      <Link href={"/"}>Tornar a l&apos;inici</Link>
    </Layout>
  );
}
