import GoToLoginOrSignup from "@/components/GoToLoginOrSignup";
import Image from "next/image";

const HomePage = () => {
  return (
    <div className="min-h-screen bg-gradient-to-r from-green-300 via-green-400 to-blue-500 flex items-center justify-center p-4">
      <div
        className="bg-white rounded-lg shadow-lg relative"
        style={{ width: "1167px", height: "771px" }}
      >
        {/* Logotip */}
        <div className="absolute left-10 top-10">
          <Image src="/5.svg" alt="Logotip" width={50} height={50} />
        </div>

        {/* Títol */}
        <h1 className="absolute top-10 left-1/2 transform -translate-x-1/2 text-3xl font-bold">
          Planifica i gestiona les teves tasques amb{" "}
          <span className="text-blue-600">Tasky</span>
        </h1>

        {/* Contingut Principal */}
        <div className="flex items-center justify-between px-20 h-full pt-24">
          {/* Columna Esquerra: Imatge i Text */}
          <div className="flex-1">
            <Image
              src="/business-tasklist.svg"
              alt="Llista de Tasques"
              width={360}
              height={360}
            />
            <p className="text-gray-600 mt-4" style={{ maxWidth: "360px" }}>
              Controla la teva feina, els terminis i els companys{" d'equip "}{" "}
              al mateix temps. Estableix i segueix terminis, assigna tasques i
              mantén els teus projectes sota control.
            </p>
          </div>

          {/* Columna Dreta: GoToLoginOrSignup Component */}
          <GoToLoginOrSignup />
        </div>
      </div>
    </div>
  );
};

export default HomePage;
