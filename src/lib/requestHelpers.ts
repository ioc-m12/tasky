import { Prisma } from "@prisma/client";
import { z } from "zod";

export function raiseError(msg: string, status: number) {
  return Response.json({ success: false, error: msg }, { status: status });
}

export function raiseSuccess(msg: string) {
  return Response.json({ success: true, message: msg }, { status: 200 });
}

export async function getSafeRequestData<Schema extends z.ZodTypeAny>(
  request: Request,
  schema: Schema,
) {
  // Check if the recived JSON match with the login schema
  const body: unknown = await request.json();
  const isValidSchema = schema.safeParse(body);
  if (!isValidSchema.success)
    throw new Error(JSON.stringify(isValidSchema.error.format()));

  return isValidSchema.data as z.infer<Schema>;
}

type GenericCreateRecord<T extends z.ZodType<any>> = (
  _data: z.infer<T>,
) => Promise<any>;
export async function makeDBOperation<Schema extends z.ZodTypeAny>(
  request: Request,
  schema: Schema,
  dbOperation: GenericCreateRecord<typeof schema>,
  errorMsg: string,
) {
  let data;
  try {
    data = await getSafeRequestData(request, schema);
  } catch (error) {
    if (error instanceof Error) return raiseError(error.message, 400);
    return raiseError("Error desconegut", 500);
  }

  try {
    const result = await dbOperation(data);
    return Response.json(result);
  } catch (error) {
    if (error instanceof Prisma.PrismaClientKnownRequestError) {
      // P2022: Unique constraint failed
      if (error.code === "P2002") {
        return raiseError(`L'identificador o camp introduït ja existeix`, 409);
      }
      return raiseError(errorMsg, 409);
    }
    return raiseError(errorMsg, 500);
  }
}
