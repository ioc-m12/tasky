import { UserSession } from "@/types";
import { EncryptJWT, base64url, jwtDecrypt } from "jose";
import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

const secretKey = process.env.SECRET_KEY;
const key = base64url.decode(secretKey!);

export async function encrypt(payload: any) {
  return await new EncryptJWT(payload)
    .setProtectedHeader({ alg: "dir", enc: "A128CBC-HS256" })
    .setIssuedAt()
    .setIssuer("tasky")
    .setAudience("clients")
    .setExpirationTime("1 day from now")
    .encrypt(key);
}

export async function decrypt(input: string): Promise<any> {
  const { payload } = await jwtDecrypt(input, key, {
    issuer: "tasky",
    audience: "clients",
  });
  return payload;
}

export async function logout() {
  // Destroy the session
  cookies().set("session", "", { expires: new Date(0) });
}

export async function getSession(): Promise<UserSession | null> {
  const session = cookies().get("session")?.value;
  if (!session) return null;
  return await decrypt(session);
}

export async function updateSession(request: NextRequest) {
  const session = request.cookies.get("session")?.value;
  if (!session) return;

  // Refresh the session so it doesn't expire
  const parsed = await decrypt(session);
  parsed.expires = createExpirationDate();
  const res = NextResponse.next();
  res.cookies.set({
    name: "session",
    value: await encrypt(parsed),
    httpOnly: true,
    expires: parsed.expires,
  });
  return res;
}

export function createExpirationDate() {
  // Return the expiration date of 1 day from now
  return new Date(Date.now() + 3600 * 1000 * 24);
}
