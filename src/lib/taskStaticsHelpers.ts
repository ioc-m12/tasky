import { GET as getStatics } from "@/app/api/protected/tasks/statics/route";
import { IStatisticsData, stateNames } from "@/types/TaskTypes";

export async function fetchStatistics() {
  const response = await getStatics();
  return await response.json();
}

// Chart options
const language = "ca-ES";
// Pie Chart related functions
export function createPieChartTaskData(statisticsData: IStatisticsData) {
  const {
    pieChartLabels,
    pieChartDataCounter,
    backgroundColors,
    hoverBackgroundColors,
  } = stateNames.reduce(
    (
      acc: {
        pieChartLabels: string[];
        pieChartDataCounter: number[];
        backgroundColors: string[];
        hoverBackgroundColors: string[];
      },
      state,
    ) => {
      const counterOfTaskByState =
        statisticsData.tasksByStatus.find(
          (status) => status.status === state.type,
        )?._count.status ?? 0;

      // Add all the static related to the state into the chart
      acc.pieChartLabels.push(state.label);
      acc.pieChartDataCounter.push(counterOfTaskByState);
      acc.backgroundColors.push(state.backgroundColor);
      acc.hoverBackgroundColors.push(state.hoverBackgroundColor);
      return acc;
    },
    {
      pieChartLabels: [],
      pieChartDataCounter: [],
      backgroundColors: [],
      hoverBackgroundColors: [],
    },
  );

  const pieChartData = {
    labels: pieChartLabels,
    datasets: [
      {
        label: "Tasques",
        data: pieChartDataCounter,
        backgroundColor: backgroundColors,
        hoverBackgroundColor: hoverBackgroundColors,
      },
    ],
  };

  const waitUntilChartIsBuilt = new Promise((resolve) => {
    setTimeout(() => {
      resolve(pieChartData);
    }, 1000);
  });
  return waitUntilChartIsBuilt;
}

// Line Chart related functions
export async function createLineChartTaskData(statisticsData: IStatisticsData) {
  const { lastSevenDays, weekTemplate } = getLastSevenDays(language);

  const taskLastDaysDatasets = stateNames.map((taskState) => {
    const dataset = {
      label: taskState.label,
      data: createTaskDatasetSpecificState(
        statisticsData,
        weekTemplate,
        taskState.type,
      ),
      fill: true,
      borderColor: taskState.backgroundColor,
      tension: 0.1,
    };
    return dataset;
  });

  const lineChartData = {
    labels: lastSevenDays,
    datasets: taskLastDaysDatasets,
  };
  const waitUntilChartIsBuilt = new Promise((resolve) => {
    setTimeout(() => {
      resolve(lineChartData);
    }, 1000);
  });
  return waitUntilChartIsBuilt;
}

function createTaskDatasetSpecificState(
  statisticsData: IStatisticsData,
  tasksPerDay: Map<string, number>,
  state: string,
) {
  for (const [date, states] of Object.entries(
    statisticsData.tasksStatusGroupedByDateOfLastWeek,
  )) {
    const taskDate = new Date(date);
    const taskCount = states[state] || 0;
    tasksPerDay.set(getDayName(language, taskDate), taskCount);
  }

  return Array.from(tasksPerDay.values());
}

function getLastSevenDays(language: string) {
  const today = new Date();
  const lastSevenDays = [];
  const weekTemplate = new Map<string, number>();

  for (let i = 0; i < 7; i++) {
    const dayName = getDayName(language, today);
    lastSevenDays.unshift(dayName);
    today.setDate(today.getDate() - 1);
  }

  lastSevenDays.map((dayName) => weekTemplate.set(dayName, 0));

  return { lastSevenDays, weekTemplate };
}

function getDayName(language: string, date: Date) {
  const formatter = new Intl.DateTimeFormat(language, { weekday: "long" });
  const dayOfWeek = formatter.format(date);
  const formattedDay = dayOfWeek.charAt(0).toUpperCase() + dayOfWeek.slice(1);
  return formattedDay;
}
