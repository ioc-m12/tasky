import nodemailer from "nodemailer";

export async function sendEmail(to: string, subject: string, message: string) {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    host: process.env.EMAIL_HOST,
    port: 587,
    secure: true,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS,
    },
  });

  const mailOption = {
    from: process.env.EMAIL_USER,
    to: to,
    subject: subject,
    html: message,
  };

  await transporter.sendMail(mailOption);
}
