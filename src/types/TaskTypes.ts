import { Prisma } from "@prisma/client";
import { z } from "zod";

const priorityValues = ["High", "Medium", "Low"] as const;
const stateValues = [
  "Pending",
  "InProgress",
  "Completed",
  "Cancelled",
] as const;

const taskWithComments = Prisma.validator<Prisma.TaskDefaultArgs>()({
  include: {
    taskComments: {
      include: {
        createdByUser: { select: { name: true } },
        assignedToUser: { select: { name: true } },
      },
    },
    user: { select: { name: true } },
  },
});

export type ITaskWithComments = Prisma.TaskGetPayload<typeof taskWithComments>;

export const priorityNames = [
  {
    type: "High",
    name: "Alta",
  },
  {
    type: "Medium",
    name: "Mitjana",
  },
  {
    type: "Low",
    name: "Baixa",
  },
];

export const stateNames = [
  {
    type: "Pending",
    name: "Pendent",
    backgroundColor: "#566ADA",
    hoverBackgroundColor: "#6273D1",
    label: "Tasques pendents",
  },
  {
    type: "InProgress",
    name: "En progrés",
    backgroundColor: "#FFD700",
    hoverBackgroundColor: "#FFA500",
    label: "Tasques en curs",
  },
  {
    type: "Completed",
    label: "Tasques realitzades",
    name: "Completat",
    backgroundColor: "#00BF63",
    hoverBackgroundColor: "#079E5F",
  },
  {
    type: "Cancelled",
    name: "Cancelat",
    label: "Tasques cancelades",
    backgroundColor: "#DC143C",
    hoverBackgroundColor: "#B22222",
  },
];

export interface IStatisticsData {
  tasksByStatus: {
    _count: { status: number };
    status: string;
  }[];
  tasksStatusGroupedByDateOfLastWeek: {
    [date: string]: {
      [status: string]: number;
    };
  };
}

export const createTaskSchema = z.object({
  name: z.string().min(2, {
    message: "És obligatori escriure un nom a la tasca.",
  }),
  dueDate: z.coerce.date({
    required_error: "És obligatori escriure una data límit",
  }),
  priority: z.enum(priorityValues),
  description: z.string({
    required_error: "És obligatori escriure una descripció.",
  }),
});

export const createTaskCommentSchema = z.object({
  taskId: z
    .number({ required_error: "El id de la tasca es obligatori" })
    .int()
    .positive(),
  comment: z.string({
    required_error: "És obligatori escriure un comentari.",
  }),
  assignedToUserId: z.optional(z.number().int().positive()),
});

export const editTaskCommentSchema = createTaskCommentSchema
  .omit({ taskId: true })
  .extend({
    commentId: z.number().int().positive(),
    assignedToUserId: z.number().int().positive(),
  });

export const editTaskSchema = createTaskSchema.extend({
  taskId: z
    .number({ required_error: "El id de la tasca es obligatori" })
    .int()
    .positive(),
  status: z.enum(stateValues),
});
