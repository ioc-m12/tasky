import { User } from "@prisma/client";
import { z } from "zod";

export const loginSchema = z.object({
  email: z.string().email("Escriu un email vàlid."),
  password: z
    .string()
    .min(6, "La contrasenya ha de ser més gran que 6 caracters.")
    .max(16, "La contrasenya excedeix la longitud màxima permesa"),
});

export const registerBusinessSchema = z.object({
  name: z
    .string({ required_error: "És obligatori escriure un nom." })
    .min(2, "Ha de ser un nom més llarg de 2 caràcters.")
    .max(150, "No permetem registrar noms més llargs de 150 caràcters."),
  address: z
    .string({ required_error: "És obligatori escriure una adreça." })
    .min(10, "Ha de ser una adreça vàlida."),
});

export const inviteUserSchema = z.object({
  email: z
    .string({ required_error: "És obligatori escriure un email." })
    .email("Escriu un email vàlid."),
});

export const registerUserSchema = z.object({
  name: z
    .string({ required_error: "És obligatori escriure un nom." })
    .min(5, "Ha de ser un nom més llarg de 5 caràcters.")
    .max(150, "No permetem registrar noms més llargs de 150 caràcters."),
  email: z
    .string({ required_error: "És obligatori escriure un email." })
    .email("Escriu un email vàlid."),
  password: z
    .string({ required_error: "És obligatori escriure una contrasenya." })
    .min(6, "La contrasenya ha de ser més gran que 6 caracters.")
    .max(16, "La contrasenya excedeix la longitud màxima permesa"),
  roleId: z
    .number({ required_error: "S'ha de seleccionar un rol vàlid." })
    .int()
    .positive(),
  businessId: z
    .number({ required_error: "S'ha de seleccionar una empresa vàlida." })
    .int()
    .positive(),
});

export const updateUserSchema = z.object({
  id: z
    .number({ required_error: "El id del usuari es obligatori" })
    .int()
    .positive(),
  isActive: z.boolean({ required_error: "Es un camp obligatori" }),
  name: z
    .string({ required_error: "És obligatori escriure un nom." })
    .min(5, "Ha de ser un nom més llarg de 5 caràcters.")
    .max(150, "No permetem registrar noms més llargs de 150 caràcters."),
  email: z
    .string({ required_error: "És obligatori escriure un email." })
    .email("Escriu un email vàlid."),
  roleId: z
    .number({ required_error: "S'ha de seleccionar un rol vàlid." })
    .int()
    .positive(),
});

export const registerCombinedSchema = z.object({
  user: registerUserSchema,
  business: registerBusinessSchema,
});

export const deleteBusinessSchema = z.object({
  id: z
    .number({ required_error: "S'ha d'especificar un id d'empresa vàlid." })
    .int()
    .positive(),
});

export const changePasswordSchema = z.object({
  token: z.string({ required_error: "És obligatori escriure un token." }),
  newPassword: z
    .string({ required_error: "És obligatori escriure una contrasenya." })
    .min(6, "La contrasenya ha de ser més gran que 6 caracters.")
    .max(16, "La contrasenya excedeix la longitud màxima permesa"),
  repeatedPassword: z
    .string({ required_error: "És obligatori escriure una contrasenya." })
    .min(6, "La contrasenya ha de ser més gran que 6 caracters.")
    .max(16, "La contrasenya excedeix la longitud màxima permesa"),
});

export const sendRecoverPasswordSchema = z.object({
  email: z
    .string({ required_error: "És obligatori escriure un email." })
    .email("Escriu un email vàlid."),
});

export const RequestRecoverPasswordSchema = z.union([
  changePasswordSchema,
  sendRecoverPasswordSchema,
]);

// Define primitive response types to make frontend more secure and type safe errors
// so we avoid doing mistakes on handle API response
type SuccessResponse = {
  success: boolean;
  message: string;
  type: "success";
};

type ErrorResponse = {
  success: boolean;
  error: string;
  type: "error";
};

type UnknownResponse = {
  type: "unknown";
};

function hasProperty(data: unknown): data is { [key: string]: unknown } {
  return data != null;
}

function isSuccessResponse(response: unknown): response is SuccessResponse {
  return (
    hasProperty(response) &&
    typeof response.success === "boolean" &&
    typeof response.message === "string"
  );
}

export function isErrorResponse(response: unknown): response is ErrorResponse {
  return (
    hasProperty(response) &&
    typeof response.success === "boolean" &&
    typeof response.error === "string"
  );
}

type ResponseType = SuccessResponse | ErrorResponse;

export function CategoriseResponseType(
  response: unknown,
): ResponseType | UnknownResponse {
  if (isSuccessResponse(response)) {
    return { ...response, type: "success" } as SuccessResponse;
  }

  if (isErrorResponse(response)) {
    return { ...response, type: "error" } as ErrorResponse;
  }

  return { type: "unknown" } as UnknownResponse;
}

export type UserSession = {
  user: User;
};
