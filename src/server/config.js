// @ts-check
const { z } = require("zod");

const Config = z.object({
  DATABASE_URL: z.string({ required_error: "Variable obligatòria" }),
  SHADOW_DATABASE_URL: z.string({ required_error: "Variable obligatòria" }),
  SECRET_KEY: z.string({ required_error: "Variable obligatòria" }),
  EMAIL_HOST: z.string({ required_error: "Variable obligatòria" }),
  EMAIL_USER: z.string({ required_error: "Variable obligatòria" }),
  EMAIL_PASS: z.string({ required_error: "Variable obligatòria" }),
});

const config = Config.parse(process.env);

module.exports.config = config;
