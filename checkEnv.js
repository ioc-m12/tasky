const requiredVariables = [
  "DATABASE_URL",
  "SHADOW_DATABASE_URL",
  "SECRET_KEY",
  "EMAIL_HOST",
  "EMAIL_USER",
  "EMAIL_PASS",
];

requiredVariables.forEach((variable) => {
  if (!process.env[variable]) {
    console.error(
      `Error: The ${variable} environment variable is empty or undefined, please contact with the admin`,
    );
    process.exit(1); // Exit with an error code
  }
});

console.info("All Variables are available");
