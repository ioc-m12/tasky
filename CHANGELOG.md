# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.1](https://gitlab.com/ioc-m12/tasky/compare/v0.1.0...v0.1.1) (2024-05-06)

### Features

- **backend/frontend:** added endpoint to edit the info of a task and adding more skeletons ([18c8bf4](https://gitlab.com/ioc-m12/tasky/commit/18c8bf4dbe7acc9dafcd427eac8006fb431068c8))
- **backend:** added endpoint to create comments of task in backend ([49e6147](https://gitlab.com/ioc-m12/tasky/commit/49e614799b8001782c425f93e6b6c06128e09b8b))
- **backend:** implemented task statics endpoint, edit task comments and fixing bugs sending emails ([28fa379](https://gitlab.com/ioc-m12/tasky/commit/28fa379a2c4388cd085298952bb50e7ba6e981e0))
- **backend:** when someone different than the author of task creates comment, send email to author ([23f2bc8](https://gitlab.com/ioc-m12/tasky/commit/23f2bc8a354bf094d4a9de466d583a339635606c))
- **frontend:** added feature to edit the last comment of a task ([c73e96e](https://gitlab.com/ioc-m12/tasky/commit/c73e96ec758127b9cecff669b23d467405c2c21e))
- **frontend:** added statistics page. New sidebar buttons and changes to managing workers page ([16114c4](https://gitlab.com/ioc-m12/tasky/commit/16114c4663eca177baa630a64fb03e48c451d1c2))
- **frontend:** created new task details page, added a new component and small changes ([dcc1ba3](https://gitlab.com/ioc-m12/tasky/commit/dcc1ba385f126831f573b329e5e9e8679954ab28))

### Bug Fixes

- **backend/frontend:** fixing code smells and improving task detail page ([7dd3107](https://gitlab.com/ioc-m12/tasky/commit/7dd3107c26c496661fa19326b5e7c87768896c19))
- **protected/users:** list all the workers of the same business ([90ee0be](https://gitlab.com/ioc-m12/tasky/commit/90ee0be66b6897725088277fdf46512e2125105f))
- **registerbusinessform:** making endpoint relative to the current domain instead of localhost ([0be8ee4](https://gitlab.com/ioc-m12/tasky/commit/0be8ee4f53bcd73e2387dd3027b3b751ea0a27e9))
- **tableskeleton:** fixed list should have a unique key ([65b5efd](https://gitlab.com/ioc-m12/tasky/commit/65b5efd024a65815d933144a7b9e90576d17971d))

## 0.1.0 (2024-04-15)

### Features

- **all:** adding create task functionality and also get task detail endpoint ([4e672e8](https://gitlab.com/ioc-m12/tasky/commit/4e672e8437b0476ceb7abf298d961c4f329dd3cc))
- **all:** creating the dashboard, 404 and list of workers page ([dd6813b](https://gitlab.com/ioc-m12/tasky/commit/dd6813b375fcac72644934ca8ac643552d5baf88))
- **all:** creating the dashboard, 404 and list of workers page ([9fefc86](https://gitlab.com/ioc-m12/tasky/commit/9fefc8601f594ece4f189c56cff6d75e0663b71a))
- **all:** working on tasks: making the db tables, first backend endpoints, list tasks ([cd67982](https://gitlab.com/ioc-m12/tasky/commit/cd67982ed5fdae9356197ed2b93b9237996dc6ab))
- **entire project:** added required env vars checker on startup ([dea4023](https://gitlab.com/ioc-m12/tasky/commit/dea4023d2fc6f9946c4c14ffa382aba2a4d625d9))
- **entire project:** creating project structure and forcing to use the same code style ([7522293](https://gitlab.com/ioc-m12/tasky/commit/7522293c48b1513aab848e72b5de14cbf1942ee1))
- **entire project:** custom IDE setup, added reset password functionality, fix db type warning ([ddf5d48](https://gitlab.com/ioc-m12/tasky/commit/ddf5d48c4ef3c20783cc5a45daeb60a80a17f7af))
- **frontend:** adapting dashboard with current user's data. Also fixing table styles ([3c8d765](https://gitlab.com/ioc-m12/tasky/commit/3c8d76560c124a2d74602dec90af88973bb151d4))
- **frontend:** created login page and establishing the bases of the frontend ([1a4abb9](https://gitlab.com/ioc-m12/tasky/commit/1a4abb9c43a0ed9a165ef7a6a5ff062d98f2ba1f))
- **frontend:** Created register business page ([41552f5](https://gitlab.com/ioc-m12/tasky/commit/41552f5a2efda9ccd374bebf4e0971108aa3a4a5))
- **frontend:** created the forgot and reset password pages, small styling changes ([5bcb1a2](https://gitlab.com/ioc-m12/tasky/commit/5bcb1a254fd52b9fad5198850f7b1b98f379e810))
- **homepage:** implementar la homepage ([9f07608](https://gitlab.com/ioc-m12/tasky/commit/9f07608a05e4bf7a6cbf9dda7ab575a2f69acafd))
- **link de homepage:** arreglar els links de la homepage ([0207e45](https://gitlab.com/ioc-m12/tasky/commit/0207e4560c2bd8abe2876ed8ab529b4ad2748f5e))
- **login/db:** implementing login authentication logic and create database tables ([6152007](https://gitlab.com/ioc-m12/tasky/commit/61520079754455caefc9c610329a686f64300bb4))
- **register worker:** add register user form ([b3009ce](https://gitlab.com/ioc-m12/tasky/commit/b3009ce41b6709857e913ee31aa50be085d8e21e))

### Bug Fixes

- **build:** fixing build errors ([7a62bd6](https://gitlab.com/ioc-m12/tasky/commit/7a62bd6fb0b5518db8450599c1c0fc3313b51240))
- **frontend/backend:** fixing forms that did not work and allow sending emails to everyone ([ed3fbca](https://gitlab.com/ioc-m12/tasky/commit/ed3fbca09e21e0fb95011ab20053233547f38a72))
